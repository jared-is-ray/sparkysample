<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky;

use Aws\CloudWatch\CloudWatchClient;
use Aws\Common\Aws;
use Aws\Ec2\Ec2Client;
use Aws\S3\S3Client;
use Aws\Sqs\SqsClient;
use Sparky\Actions\ActionInterface;
use Sparky\Actions\ActionResult;
use Sparky\Instances\Instance;
use Sparky\Instances\InstanceContext;
use Sparky\InstanceStrategies\InstanceStrategyInterface;
use Sparky\InstanceStrategies\KillStrategyInterface;
use Sparky\Scalers\ScalerInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SparkyFactory
{
    /**
     * @var Logger
     */
    private $_logger;

    /**
     * @var \Aws\Common\Aws
     */
    private $_aws;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->_logger = new Logger('sparky');

        $logHandler = new StreamHandler('php://stdout');
        $logHandler->setFormatter(new LineFormatter());
        $logHandler->setLevel($this->_getLogLevel(isset($config['logLevel']) ? $config['logLevel'] : 'DEBUG'));
        $this->_logger->pushHandler($logHandler);
        $this->_aws = isset($config['aws']) ? $config['aws'] : Aws::factory([
            'region' => 'us-east-1',
//            'request.options' => [
//                'proxy' => '127.0.0.1:8888'
//            ],
//            'ssl.certificate_authority' => false,
        ]);
    }

    /**
     * @return Ec2Client
     */
    public function getEc2()
    {
        return $this->_aws->get('ec2');
    }

    /**
     * @return CloudWatchClient
     */
    public function getCloudWatch()
    {
        return $this->_aws->get('cloudwatch');
    }

    /**
     * @return SqsClient
     */
    public function getSqs()
    {
        return $this->_aws->get('sqs');
    }

    /**
     * @return S3Client
     */
    public function getS3()
    {
        return $this->_aws->get('s3');
    }

    /**
     * @param array $config
     * @return Instances\InstanceConfig
     */
    public function getInstanceConfig($config)
    {
        return new Instances\InstanceConfig($config);
    }

    /**
     * @return ActionResult
     */
    public function getActionResultInstance()
    {
        return new ActionResult();
    }


    /**
     * @param string $type
     * @param array $config
     * @return ActionInterface
     */
    public function getActionInstance($type, $config = [])
    {
        if(strpos($type, '\\') === FALSE)
        {
            $type = '\\Sparky\\Actions\\' . $type;
        }
        if(!$config)
        {
            $config = [];
        }
        if(!isset($config['factory']))
        {
            $config['factory'] = $this;
        }
        return new $type($config);
    }

    /**
     * @param string $type
     * @param array $config
     * @return InstanceStrategyInterface
     */
    public function getStrategyInstance($type, $config = [])
    {
        if(strpos($type, '\\') === FALSE)
        {
            $type = '\\Sparky\\InstanceStrategies\\' . $type;
        }
        if(!$config)
        {
            $config = [];
        }
        if(!isset($config['factory']))
        {
            $config['factory'] = $this;
        }
        return new $type($config);
    }

    /**
     * @param string $type
     * @param array $config
     * @return ScalerInterface
     */
    public function getScalerInstance($type, $config)
    {
        if(strpos($type, '\\') === FALSE)
        {
            $type = '\\Sparky\\Scalers\\' . $type;
        }
        if(!$config)
        {
            $config = [];
        }
        if(!isset($config['factory']))
        {
            $config['factory'] = $this;
        }
        return new $type($config);
    }

    /**
     * @param string $type
     * @param array $config
     * @return \Sparky\KillStrategies\KillStrategyInterface
     */
    public function getKillStrategyInstance($type, $config)
    {
        if(strpos($type, '\\') === FALSE)
        {
            $type = '\\Sparky\\KillStrategies\\' . $type;
        }
        if(!$config)
        {
            $config = [];
        }
        if(!isset($config['factory']))
        {
            $config['factory'] = $this;
        }
        return new $type($config);
    }

    /**
     * @return Clock
     */
    public function getClockInstance()
    {
        return new Clock();
    }

    /**
     * @param string $type
     * @param array $config
     * @return InstanceContext
     */
    public function getInstanceContext($type, $config)
    {
        if(strpos($type, '\\') === FALSE)
        {
            $type = '\\Sparky\\Instances\\' . $type;
        }
        if(!$config)
        {
            $config = [];
        }
        if(!isset($config['factory']))
        {
            $config['factory'] = $this;
        }
        return new $type($config);
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getEventDispatcherInstance()
    {
        return new EventDispatcher();
    }

    /**
     * @param string $levelName
     * @return int
     */
    private function _getLogLevel($levelName)
    {
        $rc = new \ReflectionClass('\\Monolog\\Logger');
        $constants = $rc->getConstants();
        if(isset($constants[$levelName]))
        {
            return $constants[$levelName];
        }
        return Logger::DEBUG;
    }
}
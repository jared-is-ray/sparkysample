<?php
namespace Sparky\Instances;


class InstanceConfig
{
    /**
     * @var string
     */
    private $_securityGroupIds;

    /**
     * @var string
     */
    private $_ami;
    
    /**
     * @var string
     */
    private $_iamProfileArn;
    
    /**
     * @var string
     */
    private $_keyName;

    /**
     * @var string
     */
    private $_userData;
    
    /**
     * @var string
     */
    private $_instanceType;

    /**
     * @var string
     */
    private $_subnetId;
    
    /**
     * @var array
     */
    private $_blockDeviceMappings;
    
    /**
     * @var boolean
     */
    private $_monitoring;

    /**
     * @var string
     */
    private $_availabilityZone;

    /**
     * @var string
     */
    private $_publicIp;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->_ami                 = $config['ami'];
        $this->_securityGroupIds    = $config['securityGroupIds'];
        $this->_iamProfileArn       = $config['iamProfileArn'];
        $this->_keyName             = $config['keyName'];
        $this->_userData            = $config['userData'];
        $this->_instanceType        = $config['instanceType'];
        $this->_subnetId            = $config['subnetId'];
        $this->_blockDeviceMappings = $config['blockDeviceMappings'];
        $this->_monitoring          = $config['monitoring'];
        $this->_availabilityZone    = $config['availabilityZone'];
        $this->_publicIp            = isset($config['publicIp']) ? $config['publicIp'] : NULL;
    }

    /**
     * @return array
     */
    public function createRunInstancesApiParams()
    {
        $params = [
            'ImageId'             => $this->_ami,
            'MinCount'            => 1,
            'MaxCount'            => 1,
            'KeyName'             => $this->_keyName,
            'UserData'            => base64_encode($this->_userData),
            'InstanceType'        => $this->_instanceType,
            'Monitoring'          => [
                'Enabled' => $this->_monitoring
            ],
            'BlockDeviceMappings' => $this->_blockDeviceMappings,
            'IamInstanceProfile'  => [
                'Arn' => $this->_iamProfileArn
            ]
        ];

        if($this->_publicIp)
        {
            $params['NetworkInterfaces'] = [
                [
                    'DeviceIndex'              => 0,
                    'SubnetId'                 => $this->_subnetId,
                    'Groups'                   => $this->_securityGroupIds,
                    'DeleteOnTermination'      => TRUE,
                    'AssociatePublicIpAddress' => TRUE
                ]
            ];
        } else
        {
            $params['SecurityGroupIds'] = $this->_securityGroupIds;
            $params['SubnetId']         = $this->_subnetId;
        }

        return $params;
    }

    /**
     * @return array
     */
    public function createSpotInstanceApiParams()
    {
        $params = [
            'ImageId'             => $this->_ami,
            'KeyName'             => $this->_keyName,
            'SecurityGroupIds'    => $this->_securityGroupIds,
            'UserData'            => base64_encode($this->_userData),
            'InstanceType'        => $this->_instanceType,
            'Monitoring'          => [
                'Enabled' => $this->_monitoring
            ],
            'BlockDeviceMappings' => $this->_blockDeviceMappings,
            'SubnetId'            => $this->_subnetId,
            'IamInstanceProfile'  => [
                'Arn' => $this->_iamProfileArn
            ]
        ];

        if($this->_publicIp)
        {
            $params['NetworkInterfaces'] = [
                [
                    'DeviceIndex'              => 0,
                    'SubnetId'                 => $this->_subnetId,
                    'Groups'                   => $this->_securityGroupIds,
                    'DeleteOnTermination'      => TRUE,
                    'AssociatePublicIpAddress' => TRUE
                ]
            ];
        } else
        {
            $params['SecurityGroupIds'] = $this->_securityGroupIds;
            $params['SubnetId']         = $this->_subnetId;
        }

        return $params;
    }

    /**
     * @return string
     */
    public function getInstanceType()
    {
        return $this->_instanceType;
    }

    /**
     * @return string
     */
    public function getAvailabilityZone()
    {
        return $this->_availabilityZone;
    }
}
<?php
namespace Sparky\Instances;

class SpotInstanceRequest
{
    /**
     * @var string
     */
    private $_id;

    /**
     * @var string
     */
    private $_instanceId;

    /**
     * @var float
     */
    private $_price;

    /**
     * @var string
     */
    private $_type;

    /**
     * @var \DateTime
     */
    private $_createTime;

    /**
     * @var string
     */
    private $_state;

    /**
     * @var string
     */
    private $_statusCode;

    /**
     * @var \DateTime
     */
    private $_statusUpdateTime;

    /**
     * @var string
     */
    private $_statusMessage;

    /**
     * @param array $info
     * @return SpotInstanceRequest
     */
    public static function fromInfo($info)
    {
        $instance = new SpotInstanceRequest();
        $instance->populate($info);
        return $instance;
    }

    /**
     * @param array $info
     */
    public function populate($info)
    {
        if (isset($this->_id) && $this->_id !== $info['SpotInstanceRequestId'])
        {
            throw new \DomainException(sprintf('Cannot populate SpotInstanceRequest, id %s does not match incoming id %s', $info['SpotInstanceRequestId'], $this->_id));
        }

        $this->_id               = $info['SpotInstanceRequestId'];
        $this->_price            = (float)$info['SpotPrice'];
        $this->_type             = $info['Type'];
        $this->_state            = $info['State'];
        $this->_statusCode       = $info['Status']['Code'];
        $this->_statusUpdateTime = $info['Status']['UpdateTime'];
        $this->_statusMessage    = $info['Status']['Message'];
        $this->_instanceId       = isset($info['InstanceId']) ? $info['InstanceId'] : NULL;
        $this->_createTime       = new \DateTime($info['CreateTime']);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getInstanceId()
    {
        return $this->_instanceId;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->_state;
    }

    /**
     * @return string
     */
    public function getStatusCode()
    {
        return $this->_statusCode;
    }

    /**
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->_statusMessage;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->_price;
    }
}

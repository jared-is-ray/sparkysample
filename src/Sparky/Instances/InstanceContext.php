<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Instances;

use Aws\Ec2\Ec2Client;
use Sparky\Clock;
use Sparky\InstanceStrategies\Events\InstanceFound;
use Sparky\InstanceStrategies\Events\InstanceLaunchSucceeded;
use Sparky\SparkyFactory;
use Sparky\InstanceStrategies\Events\Events;
use Sparky\Utils;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InstanceContext implements \Iterator, EventSubscriberInterface
{
    /**
     * Defines Ec2 instances states that group should consider a running instance
     * @var array
     */
    protected static $_runningStates = ['pending', 'running'];

    /**
     * @var Ec2Client
     */
    private $_ec2;

    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var Instance[]|\ArrayIterator
     */
    protected $_instances;

    /**
     * @var int
     */
    protected $_countTerminating;

    /**
     * @var int
     */
    protected $_countRunning;

    /**
     * @var int
     */
    protected $_countLaunching;

    /**
     * @var EventDispatcherInterface
     */
    protected $_eventDispatcher;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var Clock
     */
    private $_clock;

    /**
     * @var \DateTime
     */
    private $_lastUpdateDateTime;

    /**
     * @var int
     */
    private $_refreshInterval = 30;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->_instances       = new \ArrayIterator();
        $this->_factory         = $config['factory'];
        $this->_eventDispatcher = $config['eventDispatcher'];
        $this->_ec2             = $this->_factory->getEc2();
        $this->_logger          = $this->_factory->getLogger();
        $this->_clock           = $this->_factory->getClockInstance();
        $this->_countLaunching  = 0;
        $this->_countRunning    = 0;
        $this->_eventDispatcher->addSubscriber($this);
    }

    /**
     * @inheritdoc
     * @uses InstanceContext::onInstanceFound()
     * @uses InstanceContext::onInstanceLaunchStarted()
     * @uses InstanceContext::onInstanceLaunchFailed()
     * @uses InstanceContext::onInstanceLaunchSucceeded()
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::INSTANCE_FOUND   => 'onInstanceFound',
            Events::LAUNCH_STARTED   => 'onInstanceLaunchStarted',
            Events::LAUNCH_FAILED    => 'onInstanceLaunchFailed',
            Events::LAUNCH_SUCCEEDED => 'onInstanceLaunchSucceeded',
            Events::TERMINATION_STARTED   => 'onInstanceTerminationStarted',
            Events::TERMINATION_FAILED    => 'onInstanceTerminationFailed',
            Events::TERMINATION_SUCCEEDED => 'onInstanceTerminationSucceeded'
        ];
    }

    /**
     * @return Instance
     */
    public function current()
    {
        return $this->_instances->current();
    }

    public function next()
    {
        $this->_instances->next();
    }

    public function key()
    {
        return $this->_instances->key();
    }

    public function valid()
    {
        return $this->_instances->valid();
    }

    public function rewind()
    {
        $this->_instances->rewind();
    }

    /**
     * @return int
     */
    public function getCountRunning()
    {
        $this->_refresh();
        return $this->_countRunning;
    }

    /**
     * @return int
     */
    public function getCountLaunching()
    {
        return $this->_countLaunching;
    }

    public function getCountTerminating()
    {
        return $this->_countTerminating;
    }

    /**
     * Refresh data from apis
     */
    private function _refresh()
    {
        $this->_clock->refresh();
        if (!$this->_lastUpdateDateTime || $this->_clock->getSecondsElapsed($this->_lastUpdateDateTime) > $this->_refreshInterval)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('refresh interval of %d sec reached', $this->_refreshInterval)));
            $this->_lastUpdateDateTime = $this->_clock->getDateTime();
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refresh waiting for next interval'));
            return; // only refresh at given interval
        }
        $this->_refreshInstances();
    }

    /**
     * Refresh instances from EC2 API
     */
    protected function _refreshInstances()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'refreshing instances'));
        try
        {
            $instanceIds = array_keys($this->_instances->getArrayCopy());
            if (count($instanceIds) > 0)
            {
                $instances = $this->_ec2->getDescribeInstancesIterator([
                    'InstanceIds' => $instanceIds
                ]);
                foreach ($instances as $instance)
                {
                    if (!isset($this->_instances[$instance['InstanceId']]))
                    {
                        $this->_instances[$instance['InstanceId']] = Instance::fromInfo($instance);
                    } else
                    {
                        $this->_instances[$instance['InstanceId']]->populate($instance);
                    }

                    if(isset($instance['SpotInstanceRequestId']) && !empty($instance['SpotInstanceRequestId']))
                    {
                        $this->_populateInstanceSpotBid(
                            $this->_instances[$instance['InstanceId']],
                            $instance['SpotInstanceRequestId']
                        );
                    }
                }
            }

        } catch (\Exception $e)
        {
            $this->_logger->addError(Utils::formatLog($this, 'instance info could not be refreshed', $e));
        }

        $countRunning  = 0;
        $instancesDown = [];
        foreach ($this as $instance)
        {
            if(in_array($instance->getState(), self::$_runningStates))
            {
                $countRunning++;
            } else
            {
                $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                    'instance %s in terminal state: %s',
                    $instance->getId(),
                    $instance->getState()
                )));
                $instancesDown[] = $instance->getId();
            }
        }

        foreach ($instancesDown as $id)
        {
            unset($this->_instances[$id]);
        }
        $this->_countRunning = $countRunning;
    }

    /**
     * @param InstanceFound $event
     */
    public function onInstanceFound(InstanceFound $event)
    {
        $this->_addInstance($event->getInstanceId());
    }

    /**
     * @param Event $event
     */
    public function onInstanceLaunchStarted(Event $event)
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'instance launch started'));
        $this->_countLaunching++;
    }

    /**
     * @param Event $event
     */
    public function onInstanceLaunchFailed(Event $event)
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'instance launch failed'));
        $this->_countLaunching--;
    }

    /**
     * @param InstanceLaunchSucceeded $event
     */
    public function onInstanceLaunchSucceeded(InstanceLaunchSucceeded $event)
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'instance launch succeeded'));
        $this->_countLaunching--;
        $this->_addInstance($event->getInstanceId());
    }

    /**
     * @param Event $event
     */
    public function onInstanceTerminationStarted(Event $event)
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'instance termination started'));
        $this->_countTerminating++;
    }

    /**
     * @param Event $event
     */
    public function onInstanceTerminationSucceeded(Event $event)
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'instance termination succeeded'));
        $this->_countTerminating--;
        $this->_refreshInstances();
    }

    /**
     * @param Event $event
     */
    public function onInstanceTerminationFailed(Event $event)
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'instance termination failed'));
        $this->_countTerminating--;
        $this->_refreshInstances();
    }

    /**
     * @param string $instanceId
     */
    protected function _addInstance($instanceId)
    {
        $this->_logger->addDebug(Utils::formatLog($this, sprintf(
            'adding instance %s; querying info',
            $instanceId
        )));

        try
        {
            $instances = $this->_ec2->getDescribeInstancesIterator([
                'InstanceIds' => [$instanceId]
            ]);

            $instances->rewind();
            if ($instances->valid())
            {
                $instanceData = $instances->current();
                if (!isset($this->_instances[$instanceId]))
                {
                    $this->_instances[$instanceId] = Instance::fromInfo($instanceData);
                    if (in_array($this->_instances[$instanceId]->getState(), self::$_runningStates))
                    {
                        $this->_countRunning++;
                    }
                } else
                {
                    $this->_instances[$instanceId]->populate($instanceData);
                }

                $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                    'instance added: %s; state=%s',
                    $instanceId,
                    $this->_instances[$instanceId]->getState()
                )));

                if(isset($instanceData['SpotInstanceRequestId']) && !empty($instanceData['SpotInstanceRequestId']))
                {
                    $this->_populateInstanceSpotBid($this->_instances[$instanceId], $instanceData['SpotInstanceRequestId']);
                }
            } else
            {
                $this->_logger->addError(Utils::formatLog($this, sprintf(
                    'instance %s could not be added, no information returned',
                    $instanceId
                )));
            }
        } catch (\Exception $e)
        {
            $this->_logger->addError(Utils::formatLog($this, 'could not add instance', $e));
        }


    }

    /**
     * @param Instance $instance
     * @param string $spotInstanceRequestId
     */
    private function _populateInstanceSpotBid($instance, $spotInstanceRequestId)
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'populating spot bid data for instance'));

        try
        {
            $bids = $this->_ec2->describeSpotInstanceRequests([
                'SpotInstanceRequestIds' => [$spotInstanceRequestId]
            ]);

            if(isset($bids['SpotInstanceRequests'][0]))
            {
                $sir = SpotInstanceRequest::fromInfo($bids['SpotInstanceRequests'][0]);
                $instance->setSpotBid($sir);
                $this->_logger->addDebug(Utils::formatLog($this, sprintf('instance populated w/ spot bid data: %s, %s',
                    $instance->getId(),
                    $spotInstanceRequestId
                )));
            } else
            {
                $this->_logger->addDebug(Utils::formatLog($this, sprintf('no info returned for spot instance request: %s',
                    $spotInstanceRequestId
                )));
            }
        } catch(\Exception $e)
        {
            $this->_logger->addError(Utils::formatLog($this, sprintf('error getting spot instance request info: %s',
                $spotInstanceRequestId
            ), $e));
        }
    }
}
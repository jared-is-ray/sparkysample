<?php
namespace Sparky\Instances;

class Instance
{
    /**
     * @var string
     */
    private $_id;

    /**
     * @var string
     */
    private $_state;
    
    /**
     * @var SpotInstanceRequest
     */
    private $_spotBid;

    /**
     * @var \DateTime
     */
    private $_launchTime;


    /**
     * @param array $info
     * @return Instance
     */
    public static function fromInfo($info)
    {
        $instance = new self();
        $instance->populate($info);
        return $instance;
    }

    /**
     * @param array $info
     */
    public function populate($info)
    {
        if(isset($this->_id) && $this->_id !== $info['InstanceId'])
        {
            throw new \DomainException(sprintf('Cannot populate Instance, id %s does not match incoming id %s', $info['InstanceId'], $this->_id));
        }

        $this->_id = $info['InstanceId'];
        $this->_state = $info['State']['Name'];
        $this->_launchTime = \DateTime::createFromFormat('Y-m-d\\TH:i:s.000\\Z', $info['LaunchTime'], new \DateTimeZone('UTC'));
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->_state;
    }

    /**
     * @return \DateTime
     */
    public function getLaunchTime()
    {
        return $this->_launchTime;
    }

    /**
     * @param SpotInstanceRequest $spotBid
     */
    public function setSpotBid($spotBid)
    {
        $this->_spotBid = $spotBid;
    }

    /**
     * @return SpotInstanceRequest
     */
    public function getSpotBid()
    {
        return $this->_spotBid;
    }
}

<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky;

class TagHelper
{
    /**
     * @var array
     */
    private $_tags;

    /**
     * @param array $tags
     */
    public function __construct($tags = [])
    {
        $this->_tags = $tags;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function addTag($key, $value)
    {
        $this->_tags[$key] = $value;
    }

    /**
     * @return array
     */
    public function createApiFilters()
    {
        $filters = [];
        foreach($this->_tags as $key => $value)
        {
            $filters[] = ['Name' => sprintf('tag:%s', $key), 'Values' => [$value]];
        }
        return $filters;
    }

    public function createApiTags()
    {
        $tags = [];
        foreach($this->_tags as $key => $value)
        {
            $tags[] = ['Key' => $key, 'Value' => $value];
        }
        return $tags;
    }
}
<?php
namespace Sparky;

use Sparky\Config\Events\ConfigurationUpdated;
use Sparky\Config\Events\Events;
use Sparky\Config\Source\SourceInterface;
use Sparky\Instances\InstanceContext;
use Sparky\InstanceStrategies\InstanceStrategyInterface;
use Sparky\InstanceStrategies\KillStrategyInterface;
use Sparky\Scalers\ScalerInterface;
use Noodlehaus\Config;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class GroupConfig
{
    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var string
     */
    private $_scalerType;

    /**
     * @var array
     */
    private $_scalerConfig;

    /**
     * @var string
     */
    private $_killStrategyType;

    /**
     * @var array
     */
    private $_killStrategyConfig;

    /**
     * @var string
     */
    private $_instanceContextType = 'InstanceContext';

    /**
     * @var string[]
     */
    private $_instanceStrategies;

    /**
     * @var array
     */
    private $_instanceConfigData;

    /**
     * @var int
     */
    private $_min = 0;

    /**
     * @var int
     */
    private $_max = 1;

    /**
     * @var EventDispatcherInterface
     */
    private $_changeEventDispatcher;

    /**
     * @var \DateTime
     */
    private $_lastUpdateDateTime;

    /**
     * @var int
     */
    private $_refreshInterval = 5;

    /**
     * @var Clock
     */
    private $_clock;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var SourceInterface
     */
    private $_source;

    /**
     * @param array|\ArrayAccess|SourceInterface $source
     * @param SparkyFactory $factory
     */
    public function __construct($source, $factory)
    {
        $this->_source                = $source;
        $this->_factory               = $factory;
        $this->_logger                = $this->_factory->getLogger();
        $this->_clock                 = $this->_factory->getClockInstance();
        $this->_changeEventDispatcher = $this->_factory->getEventDispatcherInstance();

        if($this->_source instanceof SourceInterface)
        {
            $this->_setData($this->_source->getConfig());
        } else
        {
            $this->_setData($source);
        }
    }

    /**
     * @param array|\ArrayAccess $configData
     */
    private function _setData($configData)
    {
        if(!isset($configData['scaler']))
        {
            throw new \RuntimeException('Missing required config key: scaler');
        }

        if(!isset($configData['killStrategy']))
        {
            throw new \RuntimeException('Missing required config key: killStrategy');
        }

        if(!isset($configData['instances']))
        {
            throw new \RuntimeException('Missing required config key: instances');
        }

        if(is_string($configData['scaler']))
        {
            $this->_scalerType = $configData['scaler'];
            $this->_scalerConfig = [];
        } else
        {
            $this->_scalerType = $configData['scaler']['type'];
            $this->_scalerConfig = isset($configData['scaler']['config']) ? $configData['scaler']['config'] : [];
        }

        if(is_string($configData['killStrategy']))
        {
            $this->_killStrategyType = $configData['killStrategy'];
            $this->_killStrategyConfig = [];
        } else
        {
            $this->_killStrategyType = $configData['killStrategy']['type'];
            $this->_killStrategyConfig = isset($configData['killStrategy']['config']) ? $configData['killStrategy']['config'] : [];
        }

        $this->_instanceConfigData = $configData['instances'];
        $this->_instanceStrategies = isset($configData['strategies']) ? $configData['strategies'] : [];
        if(isset($configData['min']))
        {
            $this->_min = $configData['min'];
        }
        if(isset($configData['max']))
        {
            $this->_max = $configData['max'];
        }
    }

    /**
     * @param SparkyFactory $factory
     */
    public function setFactory($factory)
    {
        $this->_factory = $factory;
    }

    /**
     * @return ScalerInterface
     */
    public function getScaler()
    {
        return $this->_factory->getScalerInstance($this->_scalerType, $this->_scalerConfig);
    }

    /**
     * @param array $config
     * @return InstanceContext
     */
    public function getInstanceContext($config)
    {
        return $this->_factory->getInstanceContext($this->_instanceContextType, $config);
    }

    /**
     * @param string $groupName
     * @param EventDispatcherInterface $eventDispatcher
     * @return InstanceStrategyInterface[]
     */
    public function getInstanceStrategies($groupName, $eventDispatcher)
    {
        $strategies = [];

        foreach($this->_instanceStrategies as $strategyName => $strategy)
        {
            $strategyInstanceConfig = isset($strategy['config']['instances']) ? $strategy['config']['instances'] : [];
            $instanceConfigData = $this->_instanceConfigData;
            $instanceConfigData = array_merge($instanceConfigData, $strategyInstanceConfig);
            $strategy['config']['instanceConfig'] = $this->_factory->getInstanceConfig($instanceConfigData);
            $strategy['config']['groupName'] = $groupName;
            $strategy['config']['eventDispatcher'] = $eventDispatcher;
            $strategy['config']['name'] = $strategyName;
            $strategies[$strategyName] = $this->_factory->getStrategyInstance($strategy['type'], $strategy['config']);
        }

        return $strategies;
    }

    /**
     * @param InstanceContext $instanceContext
     * @param EventDispatcherInterface $eventDispatcher
     * @return \Sparky\KillStrategies\KillStrategyInterface
     */
    public function getKillStrategy($instanceContext, $eventDispatcher)
    {
        $config = $this->_killStrategyConfig;
        if(!is_array($config))
        {
            $config = [];
        }
        $config['eventDispatcher'] = $eventDispatcher;
        $config['instanceContext'] = $instanceContext;
        return $this->_factory->getKillStrategyInstance($this->_killStrategyType, $config);
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getEventDispatcher()
    {
        return $this->_factory->getEventDispatcherInstance();
    }

    /**
     * @return \Monolog\Logger
     */
    public function getLogger()
    {
        return $this->_factory->getLogger();
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->_min;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->_max;
    }

    /**
     * @param EventSubscriberInterface $subscriber
     */
    public function addSubscriber($subscriber)
    {
        $this->_changeEventDispatcher->addSubscriber($subscriber);
    }

    public function refresh()
    {
        $this->_clock->refresh();
        if(!$this->_lastUpdateDateTime || $this->_clock->getSecondsElapsed($this->_lastUpdateDateTime) > $this->_refreshInterval)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('refresh interval of %d sec reached', $this->_refreshInterval)));
            $this->_lastUpdateDateTime = $this->_clock->getDateTime();
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refresh waiting for next interval'));
            return;
        }

        if($this->_source instanceof SourceInterface)
        {
            if($this->_source->isChanged())
            {
                $this->_logger->addInfo(Utils::formatLog($this, 'configuration has changed'));
                $this->_setData($this->_source->getConfig());
                $this->_changeEventDispatcher->dispatch(Events::UPDATED, new ConfigurationUpdated($this));
            }
        }
    }
}

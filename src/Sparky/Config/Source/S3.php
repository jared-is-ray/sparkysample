<?php
namespace Sparky\Config\Source;

use Aws\S3\Exception\NoSuchKeyException;
use Aws\S3\S3Client;
use Sparky\Config\Source\SourceInterface;
use Sparky\SparkyFactory;
use Sparky\Utils;
use Noodlehaus\Config;

class S3 implements SourceInterface
{

    /**
     * @var S3Client
     */
    private $_s3;

    /**
     * @var \DateTime
     */
    private $_lastModification;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;
    
    /**
     * @var \Noodlehaus\ConfigInterface
     */
    private $_config;

    /**
     * @var string
     */
    private $_bucket;

    /**
     * @var string
     */
    private $_key;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        /** @var SparkyFactory $factory */
        $factory       = $config['factory'];
        $this->_s3     = $factory->getS3();
        $this->_logger = $factory->getLogger();
        $this->_bucket = parse_url($config['uri'], PHP_URL_HOST);
        $this->_key    = parse_url($config['uri'], PHP_URL_PATH);
        $this->_config = NULL;

        if(!$this->_bucket || !$this->_key)
        {
            throw new \RuntimeException(sprintf('%s invalid uri given: %s', __METHOD__, $config['uri']));
        }

        $this->isChanged();
    }

    public function isChanged()
    {
        $apiParams = [
            'Bucket' => $this->_bucket,
            'Key'    => $this->_key
        ];

        if($this->_lastModification)
        {
            $apiParams['IfModifiedSince'] = $this->_lastModification;
        }

        $changed = FALSE;
        try
        {
            $object = $this->_s3->getObject($apiParams);
            $body = (string)$object['Body'];
            if(trim($body) != '')
            {
                $tmpName = tempnam(sys_get_temp_dir(), 'sparky-s3') . '.yml';
                file_put_contents($tmpName, $body);
                $this->_config = Config::load($tmpName);
                $changed = TRUE;
                $this->_lastModification = new \DateTime('@' . strtotime($object['LastModified']));
            }
        } catch(\Exception $e)
        {
            $this->_logger->addError(Utils::formatLog($this, 'could not load config', $e));
            if(!$this->_config)
            {
                // if we don't have config yet, don't catch exceptions
                throw $e;
            }
        }

        if(isset($tmpName))
        {
            @unlink($tmpName);
        }

        return $changed;
    }

    public function getConfig()
    {
        return $this->_config;
    }
}
<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Config\Source;


interface SourceInterface
{
    /**
     * @return bool
     */
    public function isChanged();

    /**
     * @return \Noodlehaus\Config
     */
    public function getConfig();
}
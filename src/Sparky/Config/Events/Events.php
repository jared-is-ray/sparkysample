<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Config\Events;


final class Events
{
    const UPDATED = 'sparky.config.updated';
}
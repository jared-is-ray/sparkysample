<?php
namespace Sparky\Config\Events;

use Sparky\GroupConfig;
use Symfony\Component\EventDispatcher\Event;

class ConfigurationUpdated extends Event
{
    /**
     * @var GroupConfig
     */
    private $_groupConfig;

    /**
     * @param GroupConfig $groupConfig
     */
    public function __construct(GroupConfig $groupConfig)
    {
        $this->_groupConfig = $groupConfig;
    }

    /**
     * @return GroupConfig
     */
    public function getGroupConfig()
    {
        return $this->_groupConfig;
    }
}
<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky;


use Sparky\Actions\ActionInterface;
use Sparky\Config\Source\SourceInterface;
use Sparky\Instances\InstanceContext;
use Sparky\InstanceStrategies\InstanceStrategyInterface;
use Sparky\KillStrategies\KillStrategyInterface;
use Sparky\Scalers\ScalerInterface;

class Utils
{

    /**
     * @param object $object
     * @param string $msg
     * @param \Exception $exception
     * @return string
     */
    public static function formatLog($object, $msg, $exception = NULL)
    {
        $line = NULL;
        if($exception)
        {
            $msg .= sprintf(' (%s: %s)', get_class($exception), $exception->getMessage());
        }
        if($object instanceof InstanceGroup)
        {
            $line = sprintf('group.%s %s', $object->getName(), $msg);
        } elseif($object instanceof ScalerInterface)
        {
            $line = sprintf('scaler.%s %s', self::getClassBaseName($object), $msg);
        } elseif($object instanceof ActionInterface)
        {
            $line = sprintf('action.%s %s', self::getClassBaseName($object), $msg);
        } elseif($object instanceof InstanceStrategyInterface)
        {
            $line = sprintf('strategy.%s.%s %s', self::getClassBaseName($object), $object->getName(), $msg);
        } elseif($object instanceof InstanceContext)
        {
            $line = sprintf('context.%s %s', self::getClassBaseName($object), $msg);
        } elseif($object instanceof SourceInterface)
        {
            $line = sprintf('config-source.%s %s', self::getClassBaseName($object), $msg);
        } elseif($object instanceof GroupConfig)
        {
            $line = sprintf('group-config %s', $msg);
        } elseif($object instanceof KillStrategyInterface)
        {
            $line = sprintf('kill.%s %s', self::getClassBaseName($object), $msg);
        }
        return $line !== NULL ? $line : $msg;
    }

    /**
     * @param object $object
     * @return string
     */
    public static function getClassBaseName($object)
    {
        return substr(strrchr(get_class($object), '\\'), 1);
    }

    /**
     * Returns unix timestamp from AWS xml time like 2015-03-16T23:13:45.000Z
     * @param string $timestamp
     * @return \DateTime
     */
    public static function parseXmlTimestamp($timestamp)
    {
        return \DateTime::createFromFormat('Y-m-d\\TH:i:s.000\\Z', $timestamp, new \DateTimeZone('UTC'));
    }

    /**
     * @param int $seconds
     * @return string
     */
    public static function formatSeconds($seconds)
    {
        $min = floor($seconds / 60);
        $secRemainder = $seconds % 60;
        return sprintf('%02d:%02d', $min, $secRemainder);
    }
}
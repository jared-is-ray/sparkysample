<?php
/**
 *
 * @package    
 * @subpackage 
 *
 * @author     jray
 */

namespace Sparky\Commands;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;

class SparkyCommand extends Command
{
    protected function configure()
    {
        $this->addOption('background', 'b', InputOption::VALUE_NONE, 'run process in background');
    }

    /**
     * Performs a double fork to detach the process from the shell
     * @see http://stackoverflow.com/questions/881388/what-is-the-reason-for-performing-a-double-fork-when-creating-a-daemon
     */
    protected function _daemonize()
    {
        $pid = pcntl_fork();
        if($pid == -1)
        {
            fwrite(STDOUT, "could not make first fork");
            exit();
        } elseif($pid > 0)
        {
            exit();
        }

        $pid = pcntl_fork();
        if($pid == -1)
        {
            fwrite(STDOUT, "could not make second fork");
            exit();
        } elseif($pid > 0)
        {
            exit();
        }
    }
}
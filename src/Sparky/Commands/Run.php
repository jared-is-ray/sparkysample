<?php
namespace Sparky\Commands;

use Aws\Common\Aws;
use Sparky\Config\Source\S3;
use Sparky\GroupConfig;
use Sparky\InstanceGroup;
use Sparky\SparkyFactory;
use Noodlehaus\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Run extends SparkyCommand
{
    protected function configure()
    {
        $this->setName('run')
            ->setDescription('runs sparky from configuration file')
            ->addOption('conf', NULL, InputOption::VALUE_REQUIRED, 'sparky configuration file to load');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if($input->getOption('background'))
        {
            $this->_daemonize();
        }

        $confFileName = $input->getOption('conf');
        $sparkConfig = Config::load($confFileName);

        if(!isset($sparkConfig['groupConfigurationPath']))
        {
            throw new \RuntimeException('missing required configuration key: groupConfigurationPath');
        }

        if(!isset($sparkConfig['groups']))
        {
            throw new \RuntimeException('missing required configuration key: groups');
        }

        if(!isset($sparkConfig['logLevel']))
        {
            throw new \RuntimeException('missing required configuration key: logLevel');
        }

        $groupConfigPath = $sparkConfig['groupConfigurationPath'];
        $sparkConfig['logLevel'];

        fwrite(STDOUT, sprintf("%s - sparky starting %s\n",
            date('c'),
            $input->getOption('background') ? 'in background' : 'in console'
        ));

        $factory = new SparkyFactory(['logLevel' => $sparkConfig['logLevel']]);

        $logger = $factory->getLogger();
        $logger->addNotice('starting up');

        $shuttingDown = FALSE;

        $shutdown = function() use(&$shuttingDown, $logger) {
            if(!$shuttingDown)
            {
                $logger->addNotice('beginning shutdown');
                $shuttingDown = TRUE;
            }
        };

        pcntl_signal(SIGINT, $shutdown);
        pcntl_signal(SIGTERM, $shutdown);

        $groupNames = $sparkConfig['groups'];

        /** @var InstanceGroup[]|\ArrayIterator $groups */
        $groups = new \ArrayIterator();

        /** @var GroupConfig[]|\ArrayIterator $groupConfigs */
        $groupConfigs = new \ArrayIterator();

        foreach($groupNames as $groupName)
        {
            $groupConfFile = sprintf('%s.yml', $groupName);

            if(substr($groupConfigPath, 0, 5) == 's3://')
            {
                $source = new S3([
                    'factory' => $factory,
                    'uri' => rtrim($groupConfigPath, '/') . '/' . $groupConfFile
                ]);
            } else
            {
                if(!file_exists($groupConfFile))
                {
                    throw new \RuntimeException(sprintf('cannot load config file for group: %s', $groupConfFile));
                } else
                {
                    $source = Config::load(rtrim($groupConfigPath, '/') . '/' . $groupConfFile);
                }
            }

            $groupConf = new GroupConfig($source, $factory);
            $groupConfigs[] = $groupConf;
            $groups[] = new InstanceGroup($groupName, $groupConf);
        }

        pcntl_signal_dispatch(); // catch any signals waiting before loop starts

        while(TRUE && !$shuttingDown)
        {
            sleep(2);
            $groupConfigs->rewind();
            while($groupConfigs->valid() && !$shuttingDown)
            {
                /** @var GroupConfig $groupConfig */
                $groupConfig = $groupConfigs->current();
                $groupConfig->refresh();
                $groupConfigs->next();
                pcntl_signal_dispatch();
            }

            $groups->rewind();
            while($groups->valid() && !$shuttingDown)
            {
                /** @var InstanceGroup $group */
                $group = $groups->current();
                $group->refresh();
                $groups->next();
                pcntl_signal_dispatch();
            }
        }
        $logger->addNotice('exiting');
    }
}
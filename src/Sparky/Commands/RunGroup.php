<?php
namespace Sparky\Commands;

use Aws\Common\Aws;
use Sparky\Config\Source\S3;
use Sparky\GroupConfig;
use Sparky\InstanceGroup;
use Sparky\SparkyFactory;
use Noodlehaus\Config;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RunGroup extends SparkyCommand
{
    protected function configure()
    {
        $this->setName('run-group')
            ->setDescription('runs sparky for a single instance group')
            ->addOption('s3', NULL, InputOption::VALUE_OPTIONAL, 'S3 path to load configuration from')
            ->addOption('log-level', NULL, InputOption::VALUE_OPTIONAL, 'Log level (i.e. INFO|DEBUG)')
            ->addArgument('group', InputArgument::REQUIRED, 'Sparky group name');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if($input->getOption('background'))
        {
            $this->_daemonize();
        }

        $groupName = $input->getArgument('group');
        $s3Path = $input->getOption('s3');

        $groupConfFile = sprintf('%s.yml', $groupName);

        $factory = new SparkyFactory(['logLevel' => $input->getOption('log-level')]);

        $logger = $factory->getLogger();
        $logger->addNotice('starting up');

        $shuttingDown = FALSE;

        $shutdown = function() use(&$shuttingDown, $logger) {
            if(!$shuttingDown)
            {
                $logger->addNotice('beginning shutdown');
                $shuttingDown = TRUE;
            }
        };

        pcntl_signal(SIGINT, $shutdown);
        pcntl_signal(SIGTERM, $shutdown);

        if($s3Path)
        {
            $source = new S3([
                'factory' => $factory,
                'uri' => rtrim($s3Path, '/') . '/' . $groupConfFile
            ]);
        } else
        {
            if(!file_exists($groupConfFile))
            {
                throw new \RuntimeException(sprintf('cannot load config file for group: %s', $groupConfFile));
            } else
            {
                $source = Config::load($groupConfFile);
            }
        }

        $groupConf = new GroupConfig($source, $factory);
        $group = new InstanceGroup($groupName, $groupConf);
        pcntl_signal_dispatch(); // catch any signals waiting befor loop starts
        while(TRUE && !$shuttingDown)
        {
            sleep(2);
            $group->refresh();
            $groupConf->refresh();
            // catch any signals waiting
            pcntl_signal_dispatch();
        }
        $logger->addNotice('exiting');
    }
}
<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\InstanceStrategies;


use Aws\Ec2\Ec2Client;
use Sparky\Actions\ActionInterface;
use Sparky\Clock;
use Sparky\Instances\Instance;
use Sparky\Instances\InstanceConfig;
use Sparky\InstanceStrategies\Events\Events;
use Sparky\InstanceStrategies\Events\InstanceFound;
use Sparky\InstanceStrategies\Events\InstanceLaunchSucceeded;
use Sparky\SparkyFactory;
use Sparky\TagHelper;
use Sparky\Utils;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OnDemand implements InstanceStrategyInterface
{
    /**
     * @var ActionInterface[]
     */
    private $_actionsRunning;

    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var Ec2Client
     */
    private $_ec2;

    /**
     * @var InstanceConfig
     */
    private $_instanceConfig;

    /**
     * @var EventDispatcherInterface
     */
    private $_eventDispatcher;
    
    /**
     * @var \DateTime
     */
    private $_lastUpdateDateTime;

    /**
     * @var int
     */
    private $_refreshInterval = 30;
    
    /**
     * @var Clock
     */
    private $_clock;

    /**
     * @var string
     */
    private $_groupName;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var TagHelper
     */
    private $_tags;

    public function __construct($config = [])
    {
        $this->_actionsRunning  = [];
        $this->_factory         = $config['factory'];
        $this->_instanceConfig  = $config['instanceConfig'];
        $this->_groupName       = $config['groupName'];
        $this->_name            = $config['name'];
        $this->_eventDispatcher = $config['eventDispatcher'];
        $this->_logger          = $this->_factory->getLogger();
        $this->_ec2             = $this->_factory->getEc2();
        $this->_clock           = $this->_factory->getClockInstance();
        $this->_tags            = new TagHelper();

        $this->_tags->addTag('sparky:strategy:name', $this->_name);
        $this->_tags->addTag('sparky:strategy:type', Utils::getClassBaseName($this));
        $this->_tags->addTag('sparky:group:name', $this->_groupName);
    }

    /**
     * @return void
     */
    public function init()
    {
        if(!$this->_eventDispatcher)
        {
            throw new \LogicException(sprintf('%s: cannot init, no event disptacher has been set yet', __METHOD__));
        }
        $this->_findInstances();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * @return boolean
     */
    public function isAvailable()
    {
        $this->_refresh();
        return TRUE;
    }

    public function shutdown()
    {
        // do nothing
    }

    public function launchInstance()
    {
        $this->_logger->addNotice(Utils::formatLog($this, 'executing new launch action'));
        $action = $this->_factory->getActionInstance('LaunchOnDemand', [
            'instanceConfig' => $this->_instanceConfig,
            'tags'           => $this->_tags
        ]);
        $this->_actionsRunning[] = $action;
        $action->execute();
        $this->_eventDispatcher->dispatch(Events::LAUNCH_STARTED);
    }

    /**
     * Get the status of actions and refresh ec2 sp
     */
    private function _refresh()
    {
        $this->_clock->refresh();
        if(!$this->_lastUpdateDateTime || $this->_clock->getSecondsElapsed($this->_lastUpdateDateTime) > $this->_refreshInterval)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('refresh interval of %d sec reached', $this->_refreshInterval)));
            $this->_lastUpdateDateTime = $this->_clock->getDateTime();
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refresh waiting for next interval'));
            return; // only refresh at given interval
        }
        $this->_logger->addDebug(Utils::formatLog($this, 'refreshing info'));
        $stillRunning = [];
        foreach($this->_actionsRunning as $action)
        {
            if(!$action->isRunning())
            {
                $result = $action->getResult();
                $this->_logger->addNotice(Utils::formatLog($this, 'action completed: ' . $result->toString()));
                if($result->isSuccess())
                {
                    $this->_eventDispatcher->dispatch(
                        Events::LAUNCH_SUCCEEDED,
                        new InstanceLaunchSucceeded($result->getInstanceId())
                    );
                } else
                {
                    $this->_eventDispatcher->dispatch(Events::LAUNCH_FAILED);
                }
            } else
            {
                $stillRunning[] = $action;
            }
        }
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('%d actions still running', count($stillRunning))));
        $this->_actionsRunning = $stillRunning;
    }

    /**
     * Get the status of actions and refresh ec2 sp
     */
    private function _findInstances()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'finding instances for startup'));

        try
        {
            $instances = $this->_ec2->getDescribeInstancesIterator([
                'Filters' => $this->_tags->createApiFilters()
            ]);
            foreach($instances as $instanceData)
            {
                $instance = Instance::fromInfo($instanceData);
                if(in_array($instance->getState(), ['pending', 'running']))
                {
                    $this->_eventDispatcher->dispatch(
                        Events::INSTANCE_FOUND,
                        new InstanceFound($instance->getId())
                    );
                }
            }
        } catch(\Exception $e)
        {
            $this->_logger->addError(Utils::formatLog($this, 'could not find instances', $e));
        }
    }
}
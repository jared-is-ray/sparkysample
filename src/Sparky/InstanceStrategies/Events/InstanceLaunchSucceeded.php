<?php

namespace Sparky\InstanceStrategies\Events;

use Symfony\Component\EventDispatcher\Event;

class InstanceLaunchSucceeded extends Event
{
    /**
     * @var string
     */
    private $_instanceId;

    /**
     * @param string $instanceId
     */
    public function __construct($instanceId)
    {
        $this->_instanceId = $instanceId;
    }

    /**
     * @return string
     */
    public function getInstanceId()
    {
        return $this->_instanceId;
    }
}
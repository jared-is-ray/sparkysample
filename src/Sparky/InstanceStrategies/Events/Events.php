<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\InstanceStrategies\Events;


final class Events
{
    const LAUNCH_STARTED = 'sparky.strategy.launch-started';
    const LAUNCH_SUCCEEDED = 'sparky.strategy.launch-success';
    const LAUNCH_FAILED = 'sparky.strategy.launch-failed';

    const TERMINATION_STARTED = 'sparky.kill-strategy.termination-started';
    const TERMINATION_SUCCEEDED = 'sparky.kill-strategy.termination-success';
    const TERMINATION_FAILED = 'sparky.kill-strategy.termination-failed';

    const INSTANCE_FOUND = 'sparky.strategy.instance-found';

}
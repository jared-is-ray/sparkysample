<?php

namespace Sparky\InstanceStrategies;

interface InstanceStrategyInterface
{
    /**
     * @return boolean
     */
    public function isAvailable();

    /**
     * @return void
     */
    public function launchInstance();

    /**
     * @return void
     */
    public function init();

    /**
     * @return string
     */
    public function getName();

    /**
     * Perform any cleanup necessary for before disposing of this strategy object
     * @return void
     */
    public function shutdown();
}
<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\InstanceStrategies;


use Aws\Ec2\Ec2Client;
use Sparky\Actions\ActionInterface;
use Sparky\Clock;
use Sparky\Instances\InstanceConfig;
use Sparky\Instances\SpotInstanceRequest;
use Sparky\InstanceStrategies\Events\Events;
use Sparky\InstanceStrategies\Events\InstanceFound;
use Sparky\InstanceStrategies\Events\InstanceLaunchSucceeded;
use Sparky\SparkyFactory;
use Sparky\TagHelper;
use Sparky\Utils;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SimpleSpotBid implements InstanceStrategyInterface
{
    /**
     * @var ActionInterface[]
     */
    private $_actionsRunning;

    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var Ec2Client
     */
    private $_ec2;

    /**
     * @var InstanceConfig
     */
    private $_instanceConfig;

    /**
     * @var Clock
     */
    private $_clock;

    /**
     * @var float
     */
    private $_currentPrice;

    /**
     * @var \DateTime
     */
    private $_lastUpdateDateTime;

    /**
     * @var int
     */
    private $_refreshInterval = 30;

    /**
     * @var EventDispatcherInterface
     */
    private $_eventDispatcher;

    /**
     * @var string
     */
    private $_groupName;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var TagHelper
     */
    private $_tags;

    /**
     * @var bool
     */
    private $_enabled;

    /**
     * @var int
     */
    private $_timeout = 300;

    /**
     * @var int
     */
    private $_gracePeriod = 1800;

    /**
     * @var \DateTime
     */
    private $_lastFailedLaunch;

    public function __construct($config = [])
    {
        if(!isset($config['price']))
        {
            throw new \RuntimeException(sprintf('%s configuration invalid: missing key "price"', __METHOD__));
        }

        $this->_actionsRunning       = [];
        $this->_factory              = $config['factory'];
        $this->_instanceConfig       = $config['instanceConfig'];
        $this->_eventDispatcher      = $config['eventDispatcher'];
        $this->_logger               = $this->_factory->getLogger();
        $this->_ec2                  = $this->_factory->getEc2();
        $this->_clock                = $this->_factory->getClockInstance();
        $this->_price                = (float)$config['price'];
        $this->_name                 = $config['name'];
        $this->_groupName            = $config['groupName'];
        $this->_tags                 = new TagHelper();
        $this->_enabled              = FALSE;

        if(isset($config['timeout']) && is_numeric($config['timeout']) && $config['timeout'] > 0)
        {
            $this->_timeout = intval($config['timeout']);
        }

        $this->_tags->addTag('sparky:strategy:name', $this->_name);
        $this->_tags->addTag('sparky:strategy:type', Utils::getClassBaseName($this));
        $this->_tags->addTag('sparky:group:name', $this->_groupName);
    }

    /**
     * @return void
     */
    public function init()
    {
        if(!$this->_eventDispatcher)
        {
            throw new \LogicException(sprintf('%s: cannot init, no event dispatcher has been set yet', __METHOD__));
        }
        $this->_findInstances();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    public function shutdown()
    {
        // do nothing
    }


    /**
     * @return boolean
     */
    public function isAvailable()
    {
        $this->_refresh();
        $available = FALSE;
        $reason = NULL;

        if($this->_currentPrice === NULL)
        {
            $reason = 'no pricing information available for zone yet';
        } elseif($this->_currentPrice > $this->_price)
        {
            $reason = sprintf('price for zone %f > price configured for this strategy: %f', $this->_currentPrice, $this->_price);
        } elseif($this->_lastFailedLaunch && $this->_clock->getSecondsElapsed($this->_lastFailedLaunch) < $this->_gracePeriod)
        {
            $reason = sprintf('last launch action failed less than %s sec ago; disabled with %s left',
                $this->_gracePeriod,
                Utils::formatSeconds($this->_gracePeriod - $this->_clock->getSecondsElapsed($this->_lastFailedLaunch))
            );
        } else
        {
            $reason = sprintf('price for zone %f <= price configured for this strategy: %f', $this->_currentPrice, $this->_price);
            $available = TRUE;
        }

        if(!$available && $this->_enabled)
        {
            $this->_logger->addNotice(Utils::formatLog($this, 'disabling strategy: ' . $reason));
        } elseif($available && !$this->_enabled)
        {
            $this->_logger->addNotice(Utils::formatLog($this, 'enabling strategy: ' . $reason));
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('strategy %s: %s', $available ? 'enabled' : 'disabled', $reason)));
        }
        $this->_enabled = $available;
        return $available;
    }

    public function launchInstance()
    {
        if(!$this->isAvailable())
        {
            throw new \LogicException('cannot launch instance; strategy is currently unavailable');
        }
        $this->_logger->addNotice(Utils::formatLog($this, 'executing new launch action'));
        $action                  = $this->_factory->getActionInstance('RequestSpotInstance', [
            'instanceConfig' => $this->_instanceConfig,
            'price'          => (string)$this->_price,
            'tags'           => $this->_tags,
            'timeout'        => $this->_timeout
        ]);
        $this->_actionsRunning[] = $action;
        $action->execute();
        $this->_eventDispatcher->dispatch(Events::LAUNCH_STARTED);
    }

    /**
     * Get the status of actions and refresh ec2 sp
     */
    private function _refresh()
    {
        $this->_clock->refresh();
        if(!$this->_lastUpdateDateTime || $this->_clock->getSecondsElapsed($this->_lastUpdateDateTime) > $this->_refreshInterval)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('refresh interval of %d sec reached', $this->_refreshInterval)));
            $this->_lastUpdateDateTime = $this->_clock->getDateTime();
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refresh waiting for next interval'));
            return; // only refresh at given interval
        }
        $this->_refreshZonePricing();
        $stillRunning = [];
        foreach ($this->_actionsRunning as $action)
        {
            if (!$action->isRunning())
            {
                $result = $action->getResult();
                if($result->isSuccess())
                {
                    $this->_eventDispatcher->dispatch(
                        Events::LAUNCH_SUCCEEDED,
                        new InstanceLaunchSucceeded($result->getInstanceId())
                    );
                } else
                {
                    $this->_lastFailedLaunch = $this->_clock->getDateTime();
                    $this->_eventDispatcher->dispatch(Events::LAUNCH_FAILED);
                }
                $this->_logger->addNotice(Utils::formatLog($this, 'action completed: ' . $result->toString()));
            } else
            {
                $stillRunning[] = $action;
            }
        }
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('%d actions still running', count($stillRunning))));
        $this->_actionsRunning = $stillRunning;
    }

    /**
     * Refresh the current spot pricing for zone and instance type in this launch config
     * @return void
     */
    private function _refreshZonePricing()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'refreshing zone pricing'));
        $apiParams = [
            'InstanceTypes'      => [$this->_instanceConfig->getInstanceType()],
            'ProductDescriptions' => ['Linux/UNIX (Amazon VPC)'],
            'AvailabilityZone'    => $this->_instanceConfig->getAvailabilityZone(),
            'MaxResults'         => 1
        ];
        try
        {
            $response = $this->_ec2->describeSpotPriceHistory($apiParams);
            if(count($response['SpotPriceHistory']) > 0)
            {
                $price = $response['SpotPriceHistory'][0]['SpotPrice'];
                $this->_logger->addInfo(Utils::formatLog($this, sprintf('most recent zone spot price updated: %f', $price)));
                $this->_currentPrice = $price;
            } else
            {
                $this->_logger->addInfo(Utils::formatLog($this, 'could not refresh zone pricing; no records returned by api'));
            }
        } catch(\Exception $e)
        {
            $this->_logger->addError(Utils::formatLog($this, 'could not refresh zone pricing', $e));
        }
    }

    private function _findInstances()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'finding current spot requests'));

        /** @var SpotInstanceRequest[] $bidsAlive */
        $bidsAlive = [];

        try
        {
            $bids = $this->_ec2->getDescribeSpotInstanceRequestsIterator([
                'Filters' => $this->_tags->createApiFilters()
            ]);
            foreach($bids as $bidData)
            {
                $bid = SpotInstanceRequest::fromInfo($bidData);
                // if we have instance id or open/active state
                //   - check instanceId no matter the state of the bid,
                if($bid->getInstanceId() || in_array($bid->getState(), ['open', 'active'], TRUE))
                {
                    $this->_logger->addInfo(Utils::formatLog($this, sprintf(
                        'spot request is live: %s',
                        $bid->getId()
                    )));
                    $bidsAlive[] = $bid;
                }
                else
                {
                    $this->_logger->addInfo(Utils::formatLog($this, sprintf(
                        'spot request is not open/active: %s',
                        $bid->getId()
                    )));
                }
            }
        } catch(\Exception $e)
        {
            $this->_logger->addError(Utils::formatLog($this, 'could not find instances', $e));
        }

        // restore actions for bids
        foreach($bidsAlive as $bid)
        {
            if($bid->getState() == 'open')
            {
                $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                    'restoring action for bid: %s',
                    $bid->getId()
                )));
                $action = $this->_factory->getActionInstance('RequestSpotInstance', [
                    'spotRequest'    => $bid,
                    'tags'           => $this->_tags
                ]);
                $this->_eventDispatcher->dispatch(Events::LAUNCH_STARTED);
                $this->_actionsRunning[] = $action;
                $action->execute();
            } else
            {
                $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                    'registering instance for spot bid: %s | %s',
                    $bid->getId(),
                    $bid->getInstanceId()
                )));
                $this->_eventDispatcher->dispatch(
                    Events::INSTANCE_FOUND,
                    new InstanceFound($bid->getInstanceId())
                );
            }
        }
    }
}
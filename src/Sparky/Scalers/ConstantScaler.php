<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Scalers;


class ConstantScaler  implements ScalerInterface
{
    /**
     * @var int
     */
    private $_desiredCapacity;

    public function __construct($config)
    {
        if(!isset($config['desiredCapacity']))
        {
            throw new \RuntimeException(sprintf('%s: missing configuration key: desiredCapacity'));
        }

        $this->_desiredCapacity = intval($config['desiredCapacity']);
    }
    /**
     * @return int
     */
    public function getDesiredCapacity()
    {
        return $this->_desiredCapacity;
    }

    public function shutdown()
    {
        // do nothing
    }

    public function init()
    {
        // do nothing
    }
}
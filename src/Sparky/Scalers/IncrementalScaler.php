<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Scalers;

/**
 * Scales only by incrementing or decrementing the current
 * capacity of an instance group
 */
abstract class IncrementalScaler  implements ScalerInterface
{
    /**
     * @var int
     */
    protected $_desiredCapacity = NULL;

    /**
     * @var int
     */
    protected $_min = NULL;

    /**
     * @var int
     */
    protected $_max = NULL;

    public function init()
    {
        if($this->_desiredCapacity === NULL || $this->_min === NULL || $this->_max === NULL)
        {
            throw new \LogicException(sprintf('%s must call setInitialCapacity and setLimits before init for instances of IncrementalScaler', __METHOD__));
        }
    }

    /**
     * Should be called during startup before init.
     * @param int $initialCapacity
     */
    public function setInitialCapacity($initialCapacity)
    {
        if(!is_numeric($initialCapacity) || $initialCapacity < 0)
        {
            throw new \DomainException(sprintf('%s initial capacity must integer >= 0', __METHOD__));
        }
        $this->_desiredCapacity = $initialCapacity;
    }

    /**
     * @param int $min
     * @param int $max
     */
    public function setLimits($min, $max)
    {
        $this->_min = $min;
        $this->_max = $max;
        if(isset($this->_desiredCapacity))
        {
            $this->_desiredCapacity = min($this->_max, max($this->_min, $this->_desiredCapacity));
        }
    }

    /**
     * @return int
     */
    public function getDesiredCapacity()
    {
        return $this->_desiredCapacity;
    }
}
<?php

namespace Sparky\Scalers;

interface ScalerInterface
{
    /**
     * @return int
     */
    public function getDesiredCapacity();

    /**
     * @return void
     */
    public function init();

    /**
     * Performs any cleanup before disposing of this instance
     * @return void
     */
    public function shutdown();
}
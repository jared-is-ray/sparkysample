<?php

namespace Sparky\Scalers;

use Aws\CloudWatch\CloudWatchClient;
use Sparky\Clock;
use Sparky\SparkyFactory;
use Sparky\Utils;

class CloudWatchAlarmScaler extends IncrementalScaler
{
    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var CloudWatchClient
     */
    private $_cloudWatch;

    /**
     * @var Clock
     */
    private $_clock;

    /**
     * @var \DateTime
     */
    private $_lastUpdateDateTime;

    /**
     * @var int
     */
    private $_refreshInterval = 30;

    /**
     * @var string
     */
    private $_alarmState = 'UNKNOWN';

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        if(!isset($config['factory']))
        {
            throw new \RuntimeException(sprintf('%s missing configuration key: factory', __METHOD__));
        }

        if(!isset($config['alarmName']))
        {
            throw new \RuntimeException(sprintf('%s missing configuration key: alarmName', __METHOD__));
        }

        $this->_factory    = $config['factory'];
        $this->_alarmName  = $config['alarmName'];
        $this->_cloudWatch = $this->_factory->getCloudWatch();
        $this->_clock      = $this->_factory->getClockInstance();
        $this->_logger     = $this->_factory->getLogger();

        if(isset($config['refreshInterval']) && is_numeric($config['refreshInterval']) && $config['refreshInterval'] > 0)
        {
            $this->_refreshInterval = intval($config['refreshInterval']);
        }

        // setting _lastUpdateDateTime time here forces
        // the scaler to wait at least _refreshInterval before doing anything
        $this->_lastUpdateDateTime = $this->_clock->getDateTime();
    }

    public function getDesiredCapacity()
    {
        $this->_refresh();
        return parent::getDesiredCapacity();
    }

    public function shutdown()
    {
        // do nothing
    }

    private function _refresh()
    {
        $this->_clock->refresh();
        if(!$this->_lastUpdateDateTime || $this->_clock->getSecondsElapsed($this->_lastUpdateDateTime) > $this->_refreshInterval)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('refresh interval of %d sec reached', $this->_refreshInterval)));
            $this->_lastUpdateDateTime = $this->_clock->getDateTime();
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refresh waiting for next interval'));
            return; // only refresh at given interval
        }
        $this->_logger->addDebug(Utils::formatLog($this, 'refreshing alarm info'));

        $alarmRefreshed = FALSE;

        try
        {
            $response = $this->_cloudWatch->describeAlarms([
                'AlarmNames' => [$this->_alarmName]
            ]);

            if(isset($response['MetricAlarms'][0]))
            {
                $alarmState  = $response['MetricAlarms'][0]['StateValue'];
                $stateReason = $response['MetricAlarms'][0]['StateReason'];

                if($this->_alarmState !== $alarmState)
                {
                    $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                        'alarm state has changed; alarm:%s %s => %s (%s)',
                        $this->_alarmName,
                        $this->_alarmState,
                        $alarmState,
                        $stateReason
                    )));
                }

                $this->_alarmState = $alarmState;
                $alarmRefreshed = TRUE;
            } else
            {
                $this->_logger->addInfo(Utils::formatLog($this, sprintf('api did not return info for alarm: %s', $this->_alarmName)));
            }
        } catch(\Exception $e)
        {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf('error refreshing alarm info: %s', $this->_alarmName), $e));
        }

        if($alarmRefreshed)
        {
            if($this->_alarmState === 'ALARM')
            {
                if($this->_desiredCapacity < $this->_max)
                {
                    $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                        'alarm is in ALARM state, increasing desired capacity %d => %d: %s',
                        $this->_desiredCapacity,
                        $this->_desiredCapacity + 1,
                        $this->_alarmName
                    )));
                    $this->_desiredCapacity++;
                } else
                {
                    $this->_logger->addDebug(Utils::formatLog($this, sprintf(
                        'alarm is in ALARM state; desired capacity already at maximum of %s',
                        $this->_max
                    )));
                }
            } else
            {
                if($this->_desiredCapacity > $this->_min)
                {
                    $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                        'alarm is not in ALARM state, decreasing desired capacity %d => %d: %s',
                        $this->_desiredCapacity,
                        $this->_desiredCapacity - 1,
                        $this->_alarmName
                    )));
                    $this->_desiredCapacity--;
                } else
                {
                    $this->_logger->addDebug(Utils::formatLog($this, sprintf(
                        'alarm is not in ALARM state; desired capacity already at minimum of %s',
                        $this->_min
                    )));
                }
            }
        }
    }
}
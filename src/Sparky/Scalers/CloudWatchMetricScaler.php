<?php

namespace Sparky\Scalers;

use Aws\CloudWatch\CloudWatchClient;
use Sparky\Clock;
use Sparky\ConfigurationException;
use Sparky\SparkyFactory;
use Sparky\Utils;

class CloudWatchMetricScaler extends IncrementalScaler
{
    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var CloudWatchClient
     */
    private $_cloudWatch;

    /**
     * @var Clock
     */
    private $_clock;

    /**
     * @var \DateTime
     */
    private $_lastUpdateDateTime;

    /**
     * @var int
     */
    private $_refreshInterval = 30;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;
    
    /**
     * @var string
     */
    private $_metricName;

    /**
     * @var string
     */
    private $_metricNamespace;

    /**
     * @var array
     */
    private $_metricDimensions;

    /**
     * @var string
     */
    private $_metricStatName;

    /**
     * @var int|float
     */
    private $_threshold;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        if(!isset($config['factory']))
        {
            throw new \RuntimeException(sprintf('%s missing configuration key: factory', __METHOD__));
        }

        if(!isset($config['metricName']))
        {
            throw new \RuntimeException(sprintf('%s missing configuration key: metricName', __METHOD__));
        }

        if(!isset($config['metricNamespace']))
        {
            throw new \RuntimeException(sprintf('%s missing configuration key: metricNamespace', __METHOD__));
        }

        if(!isset($config['metricStatName']))
        {
            throw new \RuntimeException(sprintf('%s missing configuration key: metricStatName', __METHOD__));
        }

        if(!isset($config['threshold']))
        {
            throw new \RuntimeException(sprintf('%s missing configuration key: threshold', __METHOD__));
        }

        $this->_factory          = $config['factory'];
        $this->_metricName       = $config['metricName'];
        $this->_metricNamespace  = $config['metricNamespace'];
        $this->_metricStatName   = $config['metricStatName'];
        $this->_threshold        = $config['threshold'];
        $this->_cloudWatch       = $this->_factory->getCloudWatch();
        $this->_clock            = $this->_factory->getClockInstance();
        $this->_logger           = $this->_factory->getLogger();

        if(isset($config['metricDimensions']))
        {
            $this->_createMetricDimensions($config['metricDimensions']);
        }

        if(isset($config['refreshInterval']) && is_numeric($config['refreshInterval']) && $config['refreshInterval'] > 0)
        {
            $this->_refreshInterval = intval($config['refreshInterval']);
        }

        // setting _lastUpdateDateTime time here forces
        // the scaler to wait at least _refreshInterval before doing anything
        $this->_lastUpdateDateTime = $this->_clock->getDateTime();
    }

    public function getDesiredCapacity()
    {
        $this->_refresh();
        return parent::getDesiredCapacity();
    }

    public function shutdown()
    {
        // do nothing
    }

    private function _refresh()
    {
        $this->_clock->refresh();
        if(!$this->_lastUpdateDateTime || $this->_clock->getSecondsElapsed($this->_lastUpdateDateTime) > $this->_refreshInterval)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('refresh interval of %d sec reached', $this->_refreshInterval)));
            $this->_lastUpdateDateTime = $this->_clock->getDateTime();
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refresh waiting for next interval'));
            return; // only refresh at given interval
        }
        $this->_logger->addDebug(Utils::formatLog($this, 'refreshing metric info'));

        $stats = $this->_getMetricStats();
        if($stats && isset($stats['Datapoints']) && isset($stats['Datapoints'][0]))
        {
            $datapoint = $stats['Datapoints'][0];
            $metricValue = $datapoint[$this->_metricStatName];
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('datapoint returned: %s=%s', $this->_metricStatName, $metricValue)));
            if($metricValue > $this->_threshold)
            {
                if($this->_desiredCapacity < $this->_max)
                {
                    $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                        'metric value %s > threshold %s; increasing capacity by 1',
                        var_export($metricValue, TRUE),
                        var_export($this->_threshold, TRUE)
                    )));
                    $this->_desiredCapacity++;
                } else
                {
                    $this->_logger->addDebug(Utils::formatLog($this, sprintf(
                        'metric value %s > threshold %s; desired capacity already at maximum of %s',
                        var_export($metricValue, TRUE),
                        var_export($this->_threshold, TRUE),
                        $this->_max
                    )));
                }
            } else
            {
                if($this->_desiredCapacity > $this->_min)
                {
                    $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                        'metric value %s <= threshold %s; decreasing capacity by 1',
                        var_export($metricValue, TRUE),
                        var_export($this->_threshold, TRUE)
                    )));
                    $this->_desiredCapacity--;
                }
                else
                {
                    $this->_logger->addDebug(Utils::formatLog($this, sprintf(
                        'metric value %s <= threshold %s; desired capacity already at minimum of %s',
                        var_export($metricValue, TRUE),
                        var_export($this->_threshold, TRUE),
                        $this->_min
                    )));
                }

            }
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'no information returned for metric'));
        }
    }

    /**
     * @return array|NULL
     */
    private function _getMetricStats()
    {
        $startTime = $this->_clock->getDateTime();
        $startTime = $startTime->sub(new \DateInterval(sprintf('PT%dS', $this->_refreshInterval)));
        $endTime = $this->_clock->getDateTime();

        try
        {
            $request = [
                'Namespace'  => $this->_metricNamespace,
                'MetricName' => $this->_metricName,
                'StartTime'  => $startTime,
                'EndTime'    => $endTime,
                'Period'     => $this->_refreshInterval,
                'Statistics' => [$this->_metricStatName]
            ];

            if(!empty($this->_metricDimensions))
            {
                $request['Dimensions'] = $this->_metricDimensions;
            }

            $stats = $this->_cloudWatch->getMetricStatistics($request);

            return $stats;
        } catch(\Exception $e) {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf('error refreshing info for metric %s: %s: %s', $this->_metricName, $e, $e->getMessage())));
        }

        return NULL;
    }

    /**
     * Validates and sets up metric dimensions configuration
     * @param array $dimensionConfig
     * @throws ConfigurationException
     */
    private function _createMetricDimensions($dimensionConfig)
    {
        $this->_metricDimensions       = [];
        if($dimensionConfig)
        {
            foreach($dimensionConfig as $dimension)
            {
                if(isset($dimension['name']) && isset($dimension['value']))
                {
                    $this->_metricDimensions[] = ['Name' => $dimension['name'], 'Value' => $dimension['value']];
                } else
                {
                    throw new ConfigurationException(sprintf('invalid metric dimension config %s; please set "name" and "value"', var_export($dimension, TRUE)));
                }
            }
        }
    }
}
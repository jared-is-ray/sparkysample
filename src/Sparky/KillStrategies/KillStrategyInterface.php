<?php

namespace Sparky\KillStrategies;

use Sparky\Instances\InstanceContext;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

interface KillStrategyInterface
{
    /**
     * @return void
     */
    public function init();

    /**
     * @return void
     */
    public function refresh();

    /**
     * @return void
     */
    public function killInstance();

    /**
     * Performs any cleanup before disposing of this instance
     * @return void
     */
    public function shutdown();
}
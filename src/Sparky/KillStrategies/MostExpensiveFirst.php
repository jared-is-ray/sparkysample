<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\KillStrategies;

use Aws\Ec2\Ec2Client;
use Sparky\Actions\ActionInterface;
use Sparky\Clock;
use Sparky\Instances\Instance;
use Sparky\Instances\InstanceContext;
use Sparky\InstanceStrategies\Events\Events;
use Sparky\KillStrategies\KillStrategyInterface;
use Sparky\SparkyFactory;
use Sparky\Utils;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MostExpensiveFirst implements KillStrategyInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $_eventDispatcher;

    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var InstanceContext
     */
    private $_instanceContext;

    /**
     * @var ActionInterface[]
     */
    private $_runningTerminations;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var Clock
     */
    private $_clock;

    /**
     * Min number of minutes since launch time that an instance will be eligible for termination
     * @var int
     */
    private $_minInstanceMinutes = 40;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->_factory         = $config['factory'];
        $this->_instanceContext = $config['instanceContext'];
        $this->_eventDispatcher = $config['eventDispatcher'];
        $this->_clock           = $this->_factory->getClockInstance();
        $this->_logger          = $this->_factory->getLogger();
        $this->_runningTerminations  = [];
    }

    /**
     * @return void
     */
    public function init()
    {

    }

    /**
     * @return void
     */
    public function refresh()
    {
        $this->_clock->refresh();
        $stillRunning = [];
        foreach($this->_runningTerminations as $instanceId => $action)
        {
            if($action->isRunning())
            {
                $stillRunning[$instanceId] = $action;
            } else
            {
                $result = $action->getResult();
                if($result->isSuccess())
                {
                    $this->_eventDispatcher->dispatch(Events::TERMINATION_SUCCEEDED);
                    $this->_logger->addNotice(Utils::formatLog($this, sprintf('action success: %s',
                        $result->toString()
                    )));
                } else
                {
                    $this->_eventDispatcher->dispatch(Events::TERMINATION_FAILED);
                    $this->_logger->addNotice(Utils::formatLog($this, sprintf('action fail: %s',
                        $result->toString()
                    )));
                }
            }
        }
        if(count($stillRunning) > 0)
        {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf('%d actions still running',
                count($stillRunning)
            )));
        }
        $this->_runningTerminations = $stillRunning;
    }

    public function shutdown()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'shutting down'));
    }


    /**
     * @return void
     */
    public function killInstance()
    {
        $this->_clock->refresh();
        /** @var Instance[] $instances */
        $instances = array_filter(iterator_to_array($this->_instanceContext), [$this, '_filterEligibleInstances']);
        if(count($instances) > 0)
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'finding most expensive eligible instance'));
            usort($instances, [$this, '_sortInstances']);

            $toKill = $instances[count($instances) - 1];
            $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                'starting terminate action for instance: %s', $toKill->getId()
            )));
            $action = $this->_factory->getActionInstance('TerminateInstance', [
                'instance' => $toKill
            ]);
            $this->_runningTerminations[$toKill->getId()] = $action->execute();
            $this->_eventDispatcher->dispatch(Events::TERMINATION_STARTED);
        } else
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'skipping instance termination, no eligible instances to terminate'));
        }
    }

    /**
     * @param Instance $a
     * @param Instance $b
     * @return int
     */
    private function _sortInstances($a, $b)
    {
        $scoreA = $this->_getInstanceExpenseScore($a);
        $scoreB = $this->_getInstanceExpenseScore($b);
        return $scoreA === $scoreB ? 0 : ($scoreA > $scoreB ? 1 : -1);
    }

    /**
     * @param Instance $instance
     * @return bool
     */
    private function _filterEligibleInstances($instance)
    {
        $terminating = isset($this->_runningTerminations[$instance->getId()]);
        if($terminating)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('instance %s already being terminated',
                $instance->getId()
            )));
        }
        $pastMinRunTime = $this->_clock->getSecondsElapsed($instance->getLaunchTime()) >= ($this->_minInstanceMinutes * 60);
        if(!$pastMinRunTime)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('instance %s not past minimum run time of %d minutes',
                $instance->getId(),
                $this->_minInstanceMinutes
            )));
        }
        return !$terminating && $pastMinRunTime;
    }

    /**
     * Returns a number score based on how expensive an instance is.
     * The higher the number, the more expensive an instance probably is.
     * @param Instance $instance
     * @return int
     */
    private function _getInstanceExpenseScore($instance)
    {
        $score = 0;
        // on-demand is usually more expensive than spot
        $score += $instance->getSpotBid() === NULL ? 1 : 0;
        return $score;
    }
}
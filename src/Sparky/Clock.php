<?php
namespace Sparky;

class Clock
{
    /**
     * Production mode: uses real system time
     */
    const MODE_REAL = 1;

    /**
     * Test mode: allows user to set time
     */
    const MODE_TEST = 2;

    /**
     * @var int
     */
    private $_mode;

    /**
     * @var int
     */
    private $_currentTime = 0;

    public function __construct($mode = self::MODE_REAL)
    {
        $this->_mode = $mode;
        $this->_currentTime = time();
    }

    /**
     * Refresh the clock with the current time
     * @return void
     */
    public function refresh()
    {
        // only update clock time if we're not in test mode
        if($this->_mode !== self::MODE_TEST)
        {
            $this->_currentTime = time();
        }
    }

    /**
     * @param \DateTime $datetime
     */
    public function setCurrentTime($datetime)
    {
        if($this->_mode != self::MODE_TEST)
        {
            throw new \LogicException(sprintf('Cannot call method %s: clock not in test mode', __METHOD__));
        }
        $this->_currentTime = $datetime->getTimestamp();
    }

    /**
     * @param int $seconds
     */
    public function addSeconds($seconds)
    {
        if($this->_mode != self::MODE_TEST)
        {
            throw new \LogicException(sprintf('Cannot call method %s: clock not in test mode', __METHOD__));
        }
        $this->_currentTime += $seconds;
    }

    /**
     * Returns the number of seconds between current clock time and $datetime
     * @param \DateTime $datetime
     * @return int
     */
    public function getSecondsElapsed($datetime)
    {
        return $this->_currentTime - $datetime->getTimestamp();
    }

    /**
     * @return string
     */
    public function getXmlTimestamp()
    {
        return gmdate('Y-m-d\\TH:i:s.000\\Z', $this->_currentTime);
    }

    /**
     * Returns unix timestamp from AWS xml time like 2015-03-16T23:13:45.000Z
     * @param string $timestamp
     * @return \DateTime
     */
    public function parseXmlTimestamp($timestamp)
    {
        return \DateTime::createFromFormat('Y-m-d\\TH:i:s.000\\Z', $timestamp, new \DateTimeZone('UTC'));
    }

    /**
     * @return \DateTime
     */
    public function getDateTime()
    {
        return new \DateTime('@' . $this->_currentTime);
    }

    /**
     * @param int $seconds
     */
    public function sleep($seconds)
    {
        if($this->_mode != self::MODE_TEST)
        {
            sleep($seconds);
        }
    }
}
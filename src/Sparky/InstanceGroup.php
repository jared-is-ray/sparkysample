<?php
namespace Sparky;

use Sparky\Config\Events\ConfigurationUpdated;
use Sparky\Config\Events\Events;
use Sparky\Instances\InstanceContext;
use Sparky\InstanceStrategies\InstanceStrategyInterface;
use Sparky\KillStrategies\KillStrategyInterface;
use Sparky\Scalers\IncrementalScaler;
use Sparky\Scalers\ScalerInterface;
use Monolog\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InstanceGroup implements EventSubscriberInterface
{
    /**
     * @var string
     */
    private $_name;

    /**
     * @var ScalerInterface
     */
    private $_scaler;

    /**
     * @var \Sparky\Instances\InstanceContext
     */
    private $_instanceContext;

    /**
     * @var InstanceStrategyInterface[]
     */
    private $_strategies = [];

    /**
     * @var Logger
     */
    private $_logger;
    
    /**
     * @var int
     */
    private $_min;
    
    /**
     * @var int
     */
    private $_max;
    
    /**
     * @var EventDispatcherInterface
     */
    private $_eventDispatcher;
    
    /**
     * @var KillStrategyInterface
     */
    private $_killStrategy;

    public static function getSubscribedEvents()
    {
        return [
            Events::UPDATED => 'onConfigurationUpdated'
        ];
    }

    /**
     * @param string $name
     * @param GroupConfig $config
     */
    public function __construct($name, $config)
    {
        $this->_name = $name;
        $this->_setLogger($config->getLogger());
        $this->_setLimits($config->getMin(), $config->getMax());
        $this->_setEventDispatcher($config->getEventDispatcher());
        $this->_setInstanceContext($config->getInstanceContext([
            'eventDispatcher' => $this->_eventDispatcher
        ]));
        $this->_setStrategies($config->getInstanceStrategies($this->_name, $this->_eventDispatcher));
        $this->_setScaler($config->getScaler());
        $this->_setKillStrategy($config->getKillStrategy($this->_instanceContext, $this->_eventDispatcher));
        $config->addSubscriber($this);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    public function refresh()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'refreshing'));
        $this->_killStrategy->refresh();
        $desiredInstances = min($this->_max, max($this->_min, $this->_scaler->getDesiredCapacity()));
        $currentCapacity = $this->_instanceContext->getCountRunning() + $this->_instanceContext->getCountLaunching();
        $toLaunch = max($desiredInstances - $currentCapacity, 0);
        $toKill   = abs(min($desiredInstances - $currentCapacity + $this->_instanceContext->getCountTerminating(), 0));

        if($toLaunch > 0)
        {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf(
                'running fewer instances than desired; desired:%d current:%d',
                $desiredInstances,
                $currentCapacity
            )));
        } elseif($toKill > 0)
        {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf(
                'running more instances than desired; desired:%d current:%d',
                $desiredInstances,
                $currentCapacity
            )));
            while($toKill > 0)
            {
                $this->_logger->addDebug(Utils::formatLog($this, 'initiating instance kill'));
                $this->_killStrategy->killInstance();
                $toKill--;
            }
        }

        foreach($this->_strategies as $stratgeyName => $strategy)
        {
            if($strategy->isAvailable())
            {
                $this->_logger->addDebug(Utils::formatLog($this, sprintf('strategy %s is available', $stratgeyName)));
                while($toLaunch > 0)
                {
                    $this->_logger->addInfo(Utils::formatLog($this, sprintf('launching instance for strategy %s', $stratgeyName)));
                    $strategy->launchInstance();
                    $toLaunch--;
                }
            } else
            {
                $this->_logger->addDebug(Utils::formatLog($this, sprintf('strategy %s is unavailable', $stratgeyName)));
            }
        }

        foreach($this->_instanceContext as $instance)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf(
                'instance: %-10s %-10s %-10s',
                $instance->getId(),
                $instance->getState(),
                $instance->getSpotBid() ? '$' . number_format($instance->getSpotBid()->getPrice(), 3) : 'On-Demand'
            )));
        }
    }

    /**
     * Called when group configuration changes
     * @param ConfigurationUpdated $event
     */
    public function onConfigurationUpdated(ConfigurationUpdated $event)
    {
        $this->_logger->addNotice(Utils::formatLog($this, 'ConfigurationUpdated event received'));
        $config = $event->getGroupConfig();

        $this->_setLogger($config->getLogger());
        $this->_setLimits($config->getMin(), $config->getMax());
        $this->_setEventDispatcher($config->getEventDispatcher());
        $this->_setInstanceContext($config->getInstanceContext([
            'eventDispatcher' => $this->_eventDispatcher
        ]));
        $this->_setStrategies($config->getInstanceStrategies($this->_name, $this->_eventDispatcher));
        $this->_setScaler($config->getScaler());
        $this->_setKillStrategy($config->getKillStrategy($this->_instanceContext, $this->_eventDispatcher));
    }

    /**
     * @param \Monolog\Logger $logger
     */
    private function _setLogger($logger)
    {
        $this->_logger = $logger;
    }

    /**
     * @param ScalerInterface $scaler
     */
    private function _setScaler($scaler)
    {
        if($this->_scaler)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('shutting down scaler: %s', Utils::getClassBaseName($this->_scaler))));
            $this->_scaler->shutdown();
        }
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('setting group scaler: %s', Utils::getClassBaseName($scaler))));
        $this->_scaler = $scaler;

        if($this->_scaler instanceof IncrementalScaler)
        {
            $initialCapacity = $this->_instanceContext->getCountRunning() + $this->_instanceContext->getCountLaunching();
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('setting scaler\'s initial capacity: %s', $initialCapacity)));
            $this->_scaler->setInitialCapacity($initialCapacity);
            $this->_scaler->setLimits($this->_min, $this->_max);
        }
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    private function _setEventDispatcher($eventDispatcher)
    {
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('setting group event dispatcher: %s', Utils::getClassBaseName($eventDispatcher))));
        $this->_eventDispatcher = $eventDispatcher;
    }

    /**
     * @param InstanceContext $instanceContext
     */
    private function _setInstanceContext($instanceContext)
    {
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('setting group instance context: %s', Utils::getClassBaseName($instanceContext))));
        $this->_instanceContext = $instanceContext;
    }

    /**
     * @param KillStrategyInterface $killStrategy
     */
    private function _setKillStrategy($killStrategy)
    {
        if($this->_killStrategy)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('shutting down kill strategy: %s', Utils::getClassBaseName($this->_killStrategy))));
            $this->_killStrategy->shutdown();
        }
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('setting group kill strategy: %s', Utils::getClassBaseName($killStrategy))));
        $this->_killStrategy = $killStrategy;
        $this->_killStrategy->init();
    }

    /**
     * @param int $min
     * @param int $max
     */
    private function _setLimits($min, $max)
    {
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('setting group min=%d max=%d', $min, $max)));
        $this->_min = $min;
        $this->_max = $max;
    }

    /**
     * @param InstanceStrategyInterface[] $strategies
     */
    private function _setStrategies($strategies)
    {
        foreach($this->_strategies as $name => $strategy)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('shutting down strategy %s (%s)', $name, Utils::getClassBaseName($strategy))));
            $strategy->shutdown();
        }
        $this->_strategies = [];
        foreach($strategies as $name => $strategy)
        {
            $this->_logger->addDebug(Utils::formatLog($this, sprintf('adding strategy %s (%s)', $name, Utils::getClassBaseName($strategy))));
            $this->_strategies[$name] = $strategy;
            $strategy->init();
        }
    }
}

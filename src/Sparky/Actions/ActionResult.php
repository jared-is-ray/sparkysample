<?php

namespace Sparky\Actions;

class ActionResult
{
    /**
     * @var string
     */
    private $_message;

    /**
     * @var boolean
     */
    private $_isSuccess = TRUE;

    /**
     * @var \Exception
     */
    private $_exception;

    /**
     * @var string
     */
    private $_instanceId;

    /**
     * @return string
     */
    public function getInstanceId()
    {
        return $this->_instanceId;
    }

    /**
     * @param string $instanceId
     */
    public function setInstanceId($instanceId)
    {
        if($this->_instanceId)
        {
            throw new \LogicException('cannot set instance id, id is already set');
        }
        $this->_instanceId = $instanceId;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->_message = $message;
    }

    /**
     * @param boolean $isSuccess
     */
    public function setIsSuccess($isSuccess)
    {
        $this->_isSuccess = $isSuccess;
    }

    /**
     * @return boolean
     */
    public function isSuccess()
    {
        return $this->_isSuccess;
    }

    /**
     * @param \Exception $exception
     */
    public function setException($exception)
    {
        $this->_exception = $exception;
    }

    /**
     * @return string
     */
    public function toString()
    {
        $message = $this->_message;
        if($this->_exception)
        {
            if($this->_message)
            {
                $message = sprintf('%s (%s: %s)', $this->_message, get_class($this->_exception), $this->_exception->getMessage());
            } else
            {
                $message = sprintf(
                    '%s: %s',
                    get_class($this->_exception),
                    $this->_exception->getMessage()
                );
            }
        }
        if(empty($message))
        {
            $message = 'no status message';
        }

        $message = sprintf('[ActionResult: %s] %s', $this->_isSuccess ? 'success' : 'failure', $message);

        return $message;
    }
}
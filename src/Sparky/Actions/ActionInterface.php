<?php

namespace Sparky\Actions;

interface ActionInterface
{
    /**
     * @return ActionInterface
     */
    public function execute();

    /**
     * @return boolean
     */
    public function isRunning();

    /**
     * @return ActionResult
     */
    public function getResult();
}
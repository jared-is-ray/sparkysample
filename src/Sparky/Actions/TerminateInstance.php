<?php

namespace Sparky\Actions;

use Aws\Emr\Enum\InstanceState;
use Sparky\Clock;
use Sparky\Instances\InstanceConfig;
use Sparky\SparkyFactory;
use Sparky\TagHelper;
use Sparky\Utils;

class TerminateInstance implements ActionInterface
{
    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var ActionResult
     */
    private $_result;

    /**
     * @var bool
     */
    private $_inProgress;
    
    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var Clock
     */
    private $_clock;
    
    /**
     * @var \Sparky\Instances\Instance
     */
    private $_instance;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->_factory        = $config['factory'];
        $this->_instance       = $config['instance'];
        $this->_result         = $this->_factory->getActionResultInstance();
        $this->_logger         = $this->_factory->getLogger();
        $this->_inProgress     = FALSE;
        $this->_clock          = $this->_factory->getClockInstance();
    }

    /**
     * @return ActionResult
     */
    public function execute()
    {
        if(!$this->_inProgress)
        {
            $this->_inProgress = TRUE;
            try
            {
                $this->_logger->addNotice(Utils::formatLog($this, sprintf(
                    'terminating instance %s', $this->_instance->getId()
                )));
                $apiData = ['InstanceIds' => [$this->_instance->getId()]];
                $this->_factory->getEc2()->terminateInstances($apiData);
            } catch (\Exception $e)
            {
                $this->_inProgress = FALSE;
                $this->_logger->addError(Utils::formatLog($this, 'terminate instance failed', $e));
                $this->_result->setIsSuccess(FALSE);
                $this->_result->setException($e);
            }
        }
        return $this;
    }

    /**
     * @return boolean
     */
    public function isRunning()
    {
        $this->_refresh();
        return $this->_inProgress;
    }

    /**
     * @return ActionResult
     */
    public function getResult()
    {
        if($this->_inProgress)
        {
            throw new \LogicException('cannot get result from action; action is still in progress');
        }
        return $this->_result;
    }

    /**
     * Refresh instance status
     */
    private function _refresh()
    {
        if(!$this->_inProgress)
        {
            return;
        }

        $this->_refreshInstanceStatus();

        if($this->_instance->getState() === 'terminated')
        {
            $this->_logger->addNotice(Utils::formatLog($this, sprintf('instance termination successful')));
            $this->_inProgress = FALSE;
            $this->_result->setIsSuccess(TRUE);
            $this->_result->setInstanceId($this->_instance->getId());
            $this->_result->setMessage('instance termination successful');
        }
    }

    /**
     * Refresh status of instance
     */
    private function _refreshInstanceStatus()
    {
        try
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refreshing status of instance'));
            $result   = $this->_factory->getEc2()->describeInstances([
                'InstanceIds' => [$this->_instance->getId()]
            ]);
            $oldState = $this->_instance->getState();
            $this->_instance->populate($result['Reservations'][0]['Instances'][0]);
            if ($oldState !== $this->_instance->getState())
            {
                $this->_logger->addInfo(Utils::formatLog($this, sprintf('instance status has changed from %s to %s',
                    var_export($oldState, TRUE),
                    var_export($this->_instance->getState(), TRUE)
                )));
            }
        } catch (\Exception $e)
        {
            $this->_logger->addInfo(Utils::formatLog($this, 'could not get status of instance', $e));
        }
    }
}
<?php

namespace Sparky\Actions;

use Sparky\Clock;
use Sparky\Instances\InstanceConfig;
use Sparky\SparkyFactory;
use Sparky\TagHelper;
use Sparky\Utils;

class LaunchOnDemand implements ActionInterface
{
    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var ActionResult
     */
    private $_result;

    /**
     * @var InstanceConfig
     */
    private $_instanceConfig;

    /**
     * @var bool
     */
    private $_inProgress;

    /**
     * @var string
     */
    private $_instanceStatus = 'not-launched';

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var Clock
     */
    private $_clock;

    /**
     * @var string
     */
    private $_instanceId;

    /**
     * @var TagHelper
     */
    private $_tags;

    /**
     * Set to TRUE once instance is tagged
     * @var bool
     */
    private $_isTagged;

    /**
     * Set to TRUE once instance is running
     * @var bool
     */
    private $_isRunning;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->_factory        = $config['factory'];
        $this->_instanceConfig = $config['instanceConfig'];
        $this->_tags           = $config['tags'];
        $this->_result         = $this->_factory->getActionResultInstance();
        $this->_clientToken    = sha1(mt_rand(0, PHP_INT_MAX));
        $this->_logger         = $this->_factory->getLogger();
        $this->_inProgress     = FALSE;
        $this->_clock          = $this->_factory->getClockInstance();
        $this->_isTagged       = FALSE;
        $this->_isRunning      = FALSE;
    }

    /**
     * @return ActionResult
     */
    public function execute()
    {
        if(!$this->_inProgress)
        {
            $this->_inProgress = TRUE;
            try
            {
                $this->_logger->addInfo(Utils::formatLog($this, 'launching instance'));
                $apiData                = $this->_instanceConfig->createRunInstancesApiParams();
                $apiData['ClientToken'] = $this->_clientToken;
                $instances              = $this->_factory->getEc2()->runInstances($apiData);
                $this->_instanceId      = $instances['Instances'][0]['InstanceId'];
                $this->_instanceStatus  = $instances['Instances'][0]['State']['Name'];
            } catch (\Exception $e)
            {
                $this->_inProgress = FALSE;
                $this->_logger->addError(Utils::formatLog($this, 'run instance request failed', $e));
                $this->_result->setIsSuccess(FALSE);
                $this->_result->setException($e);
            }

            // sleep 2 seconds to wait for instance to show up in api
            $this->_clock->sleep(2);
            if($this->_instanceId)
            {
                $this->_tagInstance();
            }
        }
        return $this;
    }

    /**
     * @return boolean
     */
    public function isRunning()
    {
        $this->_refresh();
        return $this->_inProgress;
    }

    /**
     * @return ActionResult
     */
    public function getResult()
    {
        if($this->_inProgress)
        {
            throw new \LogicException('cannot get result from action; action is still in progress');
        }
        return $this->_result;
    }

    /**
     * Refresh instance status
     */
    private function _refresh()
    {
        if(!$this->_inProgress)
        {
            return;
        }

        if(!$this->_isRunning)
        {
            $this->_refreshInstanceStatus();
        }

        if(!$this->_isTagged)
        {
            $this->_tagInstance();
        }

        // if instance is successfully tagged and running, mark action success
        if($this->_isTagged && $this->_isRunning)
        {
            $this->_logger->addNotice(Utils::formatLog($this, sprintf('instance is running; launch was successful')));
            $this->_inProgress = FALSE;
            $this->_result->setIsSuccess(TRUE);
            $this->_result->setInstanceId($this->_instanceId);
            $this->_result->setMessage('instance launched successfully');
        }
    }

    /**
     * Refresh status of instance
     */
    private function _refreshInstanceStatus()
    {
        try
        {
            $this->_logger->addDebug(Utils::formatLog($this, 'refreshing status of instance'));
            $result = $this->_factory->getEc2()->describeInstances([
                'Filters' => [
                    [
                        'Name' => 'client-token',
                        'Values' => [$this->_clientToken]
                    ]]]);
            $status = $result['Reservations'][0]['Instances'][0]['State']['Name'];
            $launchTime = $this->_clock->parseXmlTimestamp($result['Reservations'][0]['Instances'][0]['LaunchTime']);
            if($this->_instanceStatus !== $status)
            {
                $this->_logger->addInfo(Utils::formatLog(
                    $this,
                    sprintf('instance status has changed from %s to %s', var_export($this->_instanceStatus, TRUE), var_export($status, TRUE))
                ));

                if($status  == 'running')
                {
                    $this->_isRunning = TRUE;
                }
            } elseif($status == 'pending')
            {
                $this->_clock->refresh();
                $timeDiff = $this->_clock->getSecondsElapsed($launchTime);
                if($timeDiff > 60)
                {
                    $this->_logger->notice(Utils::formatLog(
                        $this,
                        sprintf('instance has been pending for more than %s sec', $timeDiff)
                    ));
                }
            }

            $this->_instanceStatus = $status;
        } catch(\Exception $e)
        {
            $this->_logger->addInfo(Utils::formatLog($this, 'could not get status of instance', $e));
        }
    }

    /**
     * Tags the instance with sparky info
     */
    private function _tagInstance()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'attempting to tag instance'));
        if(!$this->_isTagged)
        {
            try
            {
                $this->_factory->getEc2()->createTags([
                    'Resources' => [$this->_instanceId],
                    'Tags'      => $this->_tags->createApiTags()
                ]);
                $this->_logger->addDebug(Utils::formatLog($this, 'instance tagged'));
                $this->_isTagged = TRUE;
            } catch(\Exception $e)
            {
                $this->_logger->addDebug(Utils::formatLog($this, 'tagging instance failed', $e));
            }
        }
    }
}
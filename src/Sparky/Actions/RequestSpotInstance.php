<?php

namespace Sparky\Actions;

use Sparky\Clock;
use Sparky\Instances\InstanceConfig;
use Sparky\Instances\SpotInstanceRequest;
use Sparky\SparkyFactory;
use Sparky\TagHelper;
use Sparky\Utils;

class RequestSpotInstance implements ActionInterface
{
    /**
     * @see http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/spot-bid-status.html
     *
     * This is a list of spot-request statuses that indicate a spot request may still
     * be fulfilled. We should wait for instance id when a spot request goes into one of these statuses.
     *
     * @var array
     */
    private static $_fulfillingStatus = [
        'pending-fulfillment',
        'fulfilled',
        'request-canceled-and-instance-running',
        'marked-for-termination',
    ];

    /**
     * @var SparkyFactory
     */
    private $_factory;

    /**
     * @var ActionResult
     */
    private $_result;

    /**
     * @var InstanceConfig
     */
    private $_instanceConfig;

    /**
     * @var bool
     */
    private $_inProgress;

    /**
     * @var \Monolog\Logger
     */
    private $_logger;

    /**
     * @var Clock
     */
    private $_clock;
    
    /**
     * @var float
     */
    private $_spotPrice = '.01';

    /**
     * @var TagHelper
     */
    private $_tags;

    /**
     * @var bool
     */
    private $_isTagged;

    /**
     * @var bool
     */
    private $_isInstanceTagged;
    
    /**
     * @var bool
     */
    private $_instanceId = NULL;

    /**
     * @var bool
     */
    private $_isOpen;

    /**
     * @var bool
     */
    private $_isPendingFulfillment;

    /**
     * @var SpotInstanceRequest
     */
    private $_spotRequest;

    /**
     * @var int
     */
    private $_timeout = 0;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        if(isset($config['spotRequest']))
        {
            $this->_isTagged    = TRUE;
            $this->_spotRequest = $config['spotRequest'];
        } else
        {
            $this->_isTagged       = FALSE;
            $this->_spotPrice      = $config['price'];
            $this->_instanceConfig = $config['instanceConfig'];
            $this->_timeout        = $config['timeout'];
        }
        $this->_factory              = $config['factory'];
        $this->_tags                 = $config['tags'];
        $this->_result               = $this->_factory->getActionResultInstance();
        $this->_logger               = $this->_factory->getLogger();
        $this->_clock                = $this->_factory->getClockInstance();
        $this->_isPendingFulfillment = TRUE;
        $this->_isOpen               = TRUE;
        $this->_inProgress           = FALSE;
    }

    /**
     * @return ActionResult
     */
    public function execute()
    {
        if(!$this->_inProgress)
        {
            $this->_inProgress = TRUE;

            if($this->_spotRequest)
            {
                $this->_logger->addInfo(Utils::formatLog($this, 'spot request already in progress'));
            } else
            {
                try
                {
                    $this->_logger->addInfo(Utils::formatLog($this, 'requesting spot instance'));

                    $validUntil = $this->_clock->getDateTime()->add(new \DateInterval('PT' . $this->_timeout . 'S'));

                    $apiData = [
                        'SpotPrice'           => $this->_spotPrice,
                        'LaunchSpecification' => $this->_instanceConfig->createSpotInstanceApiParams(),
                        'ValidUntil'          => $validUntil
                    ];

                    $response = $this->_factory->getEc2()->requestSpotInstances($apiData);
                    $this->_spotRequest = SpotInstanceRequest::fromInfo($response['SpotInstanceRequests'][0]);
                } catch (\Exception $e)
                {
                    $this->_inProgress = FALSE;
                    $this->_logger->addInfo(Utils::formatLog($this, 'error requesting spot instance', $e));
                    $this->_result->setIsSuccess(FALSE);
                    $this->_result->setException($e);
                }

                // wait for bid to show up in api
                $this->_clock->sleep(2);
                if($this->_spotRequest)
                {
                    $this->_tagBid();
                }
            }
        }
        return $this;
    }

    /**
     * @return boolean
     */
    public function isRunning()
    {
        $this->_refresh();
        return $this->_inProgress;
    }

    /**
     * @return ActionResult
     */
    public function getResult()
    {
        if($this->_inProgress)
        {
            throw new \LogicException('cannot get result from action; action is still in progress');
        }
        return $this->_result;
    }

    /**
     * Refresh status of this action
     */
    private function _refresh()
    {
        if(!$this->_inProgress)
        {
            return;
        }

        if(!$this->_isTagged)
        {
            $this->_tagBid();
        }

        if(!$this->_instanceId && $this->_isPendingFulfillment)
        {
            $this->_refreshBidStatus();
        }

        if($this->_instanceId && !$this->_isInstanceTagged)
        {
            $this->_tagInstance();
        }

        if($this->_instanceId && $this->_isTagged && $this->_isInstanceTagged)
        {
            $message = sprintf(
                'spot request has instance, will track this instance; state:%s status:%s message:%s',
                var_export($this->_spotRequest->getState(), TRUE),
                var_export($this->_spotRequest->getStatusCode(), TRUE),
                $this->_spotRequest->getStatusCode()
            );
            $this->_logger->addNotice(Utils::formatLog($this, $message));
            $this->_result->setInstanceId($this->_spotRequest->getInstanceId());
            $this->_result->setIsSuccess(TRUE);
            $this->_result->setMessage($message);
            $this->_inProgress = FALSE;
        } elseif(!$this->_isOpen && !$this->_isPendingFulfillment)
        {
            $message = sprintf(
                'spot request terminated; state:%s status:%s message:%s',
                var_export($this->_spotRequest->getState(), TRUE),
                var_export($this->_spotRequest->getStatusCode(), TRUE),
                $this->_spotRequest->getStatusCode()
            );
            $this->_logger->addNotice(Utils::formatLog($this, $message));
            $this->_result->setIsSuccess(FALSE);
            $this->_result->setMessage($message);
            $this->_inProgress = FALSE;
        }
    }

    /**
     * Call EC2 to update status of spot request
     */
    private function _refreshBidStatus()
    {
        $this->_logger->addDebug(Utils::formatLog($this, sprintf('refreshing spot request status: %s', var_export($this->_spotRequest->getId(), TRUE))));

        $response = $this->_factory->getEc2()->describeSpotInstanceRequests([
            'SpotInstanceRequestIds' => [$this->_spotRequest->getId()]
        ]);

        $newState = $response['SpotInstanceRequests'][0]['State'];
        $newStatusCode = $response['SpotInstanceRequests'][0]['Status']['Code'];
        $instanceId = NULL;
        if(isset($response['SpotInstanceRequests'][0]['InstanceId']) && !empty($response['SpotInstanceRequests'][0]['InstanceId']))
        {
            $instanceId =  $response['SpotInstanceRequests'][0]['InstanceId'];
        }

        if($newState !== $this->_spotRequest->getState())
        {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf('spot request state changed from %s to %s', var_export($this->_spotRequest->getState(), TRUE), var_export($newState, TRUE))));
        }

        if($newStatusCode !== $this->_spotRequest->getStatusCode())
        {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf('spot status code changed from %s to %s', var_export($this->_spotRequest->getStatusCode(), TRUE), var_export($newStatusCode, TRUE))));
        }

        if($instanceId != $this->_spotRequest->getInstanceId())
        {
            $this->_logger->addInfo(Utils::formatLog($this, sprintf('spot request has instance id: %s', $instanceId)));
            $this->_instanceId = $instanceId;
        }

        $this->_isOpen = $newState === 'open';
        $this->_isPendingFulfillment = $this->_isOpen || in_array($newStatusCode, self::$_fulfillingStatus);

        $this->_spotRequest->populate($response['SpotInstanceRequests'][0]);
    }

    /**
     * Tags the spot bid with sparky info
     */
    private function _tagBid()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'attempting to tag spot bid'));
        if(!$this->_isTagged)
        {
            try
            {
                $this->_factory->getEc2()->createTags([
                    'Resources' => [$this->_spotRequest->getId()],
                    'Tags'      => $this->_tags->createApiTags()
                ]);
                $this->_logger->addDebug(Utils::formatLog($this, 'spot bid tagged'));
                $this->_isTagged = TRUE;
            } catch(\Exception $e)
            {
                $this->_logger->addDebug(Utils::formatLog($this, 'tagging spot bid failed', $e));
            }
        }
    }

    /**
     * Tags the instance with sparky info
     */
    private function _tagInstance()
    {
        $this->_logger->addDebug(Utils::formatLog($this, 'attempting to tag instance'));
        if(!$this->_isInstanceTagged)
        {
            try
            {
                $this->_factory->getEc2()->createTags([
                    'Resources' => [$this->_instanceId],
                    'Tags'      => $this->_tags->createApiTags()
                ]);
                $this->_logger->addDebug(Utils::formatLog($this, 'instance tagged'));
                $this->_isInstanceTagged = TRUE;
            } catch(\Exception $e)
            {
                $this->_logger->addDebug(Utils::formatLog($this, 'tagging instance failed', $e));
            }
        }
    }
}
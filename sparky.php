#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use Sparky\Commands;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new Commands\RunGroup());
$application->add(new Commands\Run());
$application->run();
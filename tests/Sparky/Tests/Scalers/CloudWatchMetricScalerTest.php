<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Scalers;


use Sparky\Clock;
use Sparky\Scalers\CloudWatchMetricScaler;
use Sparky\Tests\Helpers\CloudWatchMockHelper;
use Sparky\Tests\Helpers\GetMetricStatisticsResponseMocker;
use Sparky\Tests\SparkyTestFactory;

class CloudWatchMetricScalerTest extends \PHPUnit_Framework_TestCase
{
    public function testIt()
    {
        $mockCw = new CloudWatchMockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setCloudWatch($mockCw->getClient());

        $refreshInterval = 600;

        //$clock->addSeconds(-3600);

        $scaler = new CloudWatchMetricScaler([
            'factory'     => $factory,
            'metricName'  => 'NetworkIn',
            'metricNamespace'  => 'AWS/EC2',
            'metricDimensions' => [
                [
                    'name'  => 'InstanceId',
                    'value' => 'i-7effaba8'
                ]
            ],
            'metricStatName' => 'Average',
            'threshold'      => 1,
            'refreshInterval' => $refreshInterval
        ]);

        try
        {
            $scaler->init();
            self::fail('CloudWatchMetricScaler failed to throw exception');
        } catch(\LogicException $e) {}

        $scaler->setInitialCapacity(1);
        $scaler->setLimits(1, 2);
        $scaler->init();

        // test increase
        $mockCw->addResponse(200, GetMetricStatisticsResponseMocker::make()
            ->addDatapoint('Average', 1.1)
            ->getXml());
        $clock->addSeconds($refreshInterval + 1);
        self::assertEquals(2, $scaler->getDesiredCapacity());

        // test max
        $mockCw->addResponse(200, GetMetricStatisticsResponseMocker::make()
            ->addDatapoint('Average', 1.1)
            ->getXml());
        $clock->addSeconds($refreshInterval + 1);
        self::assertEquals(2, $scaler->getDesiredCapacity());

        // test decrease
        $mockCw->addResponse(200, GetMetricStatisticsResponseMocker::make()
            ->addDatapoint('Average', .78)
            ->getXml());
        $clock->addSeconds($refreshInterval + 1);
        self::assertEquals(1, $scaler->getDesiredCapacity());

        // test min
        $mockCw->addResponse(200, GetMetricStatisticsResponseMocker::make()
            ->addDatapoint('Average', .78)
            ->getXml());
        $clock->addSeconds($refreshInterval + 1);
        self::assertEquals(1, $scaler->getDesiredCapacity());
    }
}

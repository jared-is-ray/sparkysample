<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\Scalers;


use Sparky\Clock;
use Sparky\Scalers\CloudWatchAlarmScaler;
use Sparky\Tests\Helpers\CloudWatchMockHelper;
use Sparky\Tests\SparkyTestFactory;

class CloudWatchAlarmScalerTest extends \PHPUnit_Framework_TestCase
{
    public function testIt()
    {
        $mockCw = new CloudWatchMockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setCloudWatch($mockCw->getClient());

        $refreshInterval = 600;

        $scaler = new CloudWatchAlarmScaler([
            'factory'    => $factory,
            'alarmName'  => 'some-alarm',
            'refreshInterval' => $refreshInterval
        ]);

        try
        {
            $scaler->init();
            self::fail('CloudWatchAlarmScaler failed to throw exception');
        } catch(\LogicException $e) {}

        $scaler->setInitialCapacity(1);
        $scaler->setLimits(0, 2);
        $scaler->init();

        self::assertEquals(1, $scaler->getDesiredCapacity());
        self::assertEquals(1, $scaler->getDesiredCapacity());

        $clock->addSeconds($refreshInterval + 1);
        $mockCw->addResponse(200, __DIR__ . '/Responses/describe-alarms-success.xml', [
            'state-value' => 'OK'
        ]);

        self::assertEquals(0, $scaler->getDesiredCapacity());

        $clock->addSeconds($refreshInterval + 1);
        $mockCw->addResponse(200, __DIR__ . '/Responses/describe-alarms-success.xml', [
            'state-value' => 'ALARM'
        ]);
        self::assertEquals(1, $scaler->getDesiredCapacity());

        $clock->addSeconds($refreshInterval + 1);
        $mockCw->addResponse(200, __DIR__ . '/Responses/describe-alarms-success.xml', [
            'state-value' => 'ALARM'
        ]);
        self::assertEquals(2, $scaler->getDesiredCapacity());
        self::assertEquals(2, $scaler->getDesiredCapacity());

        // no API response should cause scaler to skip scaling for that round
        $clock->addSeconds($refreshInterval + 1);
        $mockCw->addResponse(200, __DIR__ . '/Responses/describe-alarms-empty.xml');
        self::assertEquals(2, $scaler->getDesiredCapacity());

        $clock->addSeconds($refreshInterval + 1);
        $mockCw->addResponse(200, __DIR__ . '/Responses/describe-alarms-success.xml', [
            'state-value' => 'ALARM'
        ]);
        self::assertEquals(2, $scaler->getDesiredCapacity());
    }

    public function testMinInitialCapacity()
    {
        $mockCw = new CloudWatchMockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setCloudWatch($mockCw->getClient());

        $scaler = new CloudWatchAlarmScaler([
            'factory'    => $factory,
            'alarmName'  => 'some-alarm',
            'refreshInterval' => 100
        ]);

        $scaler->setInitialCapacity(0);
        $scaler->setLimits(1, 2);
        $scaler->init();

        // assert limits honored for initial capacity
        self::assertEquals(1, $scaler->getDesiredCapacity());
    }

    public function testMaxInitialCapacity()
    {
        $mockCw = new CloudWatchMockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setCloudWatch($mockCw->getClient());

        $scaler = new CloudWatchAlarmScaler([
            'factory'    => $factory,
            'alarmName'  => 'some-alarm',
            'refreshInterval' => 100
        ]);

        $scaler->setInitialCapacity(12);
        $scaler->setLimits(0, 2);
        $scaler->init();

        // assert limits honored for initial capacity
        self::assertEquals(2, $scaler->getDesiredCapacity());
    }
}

<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Helpers;

use Aws\Common\Aws;
use Aws\Ec2\Ec2Client;
use Aws\S3\S3Client;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;

/**
 * Provides some basics for mocking S3 responses
 */
class S3MockHelper
{
    /**
     * @var MockPlugin
     */
    protected $_mockPlugin;
    /**
     * @var S3Client
     */
    protected $_s3;

    /**
     * Setup Ec2Client that can mock responses
     */
    public function __construct()
    {
        $this->_mockPlugin = new MockPlugin();
        $this->_s3        = Aws::factory([
            'region'          => 'us-east-1',
            'client.backoff'  => FALSE,
            'request.options' => [
                'proxy' => '127.0.0.1:8888'
            ]
        ])->get('s3');
        $this->_s3->setSslVerification(FALSE);
        $this->_s3->addSubscriber($this->_mockPlugin);
    }

    /**
     * @return S3Client
     */
    public function getS3Client()
    {
        return $this->_s3;
    }

    /**
     * @param int $status
     * @param string $responseData
     * @param string $lastModified
     * @param array $replace
     */
    public function addResponse($status, $responseData = NULL, $lastModified)
    {
        $response = new Response($status);
        $response->setBody($responseData);
        $response->addHeader('Last-Modified', $lastModified);
        $this->_mockPlugin->addResponse($response);
    }
}
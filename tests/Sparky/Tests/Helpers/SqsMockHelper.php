<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Helpers;

use Aws\CloudWatch\CloudWatchClient;
use Aws\Common\Aws;
use Aws\Sqs\SqsClient;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;

/**
 * Provides some basics for mocking Sqs responses
 */
class SqsMockHelper
{
    /**
     * @var MockPlugin
     */
    protected $_mockPlugin;
    /**
     * @var SqsClient
     */
    protected $_sqs;

    /**
     * Setup Ec2Client that can mock responses
     */
    public function __construct()
    {
        $this->_mockPlugin = new MockPlugin();
        $this->_sqs = Aws::factory([
            'region'          => 'us-east-1',
            'client.backoff'  => FALSE,
            'request.options' => [
                'proxy' => '127.0.0.1:8888'
            ]
        ])->get('sqs');
        $this->_sqs->setSslVerification(FALSE);
        //$this->_sqs->addSubscriber($this->_mockPlugin);
    }

    /**
     * @return SqsClient
     */
    public function getClient()
    {
        return $this->_sqs;
    }

    /**
     * @param int $status
     * @param string $responseData
     * @param array $replace
     */
    public function addResponse($status, $responseData = NULL, $replace = [])
    {
        $response = new Response($status);
        if(is_string($responseData))
        {
            if(substr($responseData, 0, 5) == '<?xml')
            {
                $response->setBody($responseData);
            } else
            {
                $response->setBody($this->_loadResponse($responseData, $replace));
            }
        }
        $this->_mockPlugin->addResponse($response);
    }

    /**
     * Loads a fake XML response
     *
     * @param string $file
     * @param array $replace
     * @return string
     */
    protected function _loadResponse($file, $replace = [])
    {
        $contents = file_get_contents($file);
        foreach ($replace as $key => $value)
        {
            $contents = str_replace('%' . $key . '%', $value, $contents);
        }
        return $contents;
    }
}
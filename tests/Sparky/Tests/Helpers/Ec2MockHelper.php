<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Helpers;

use Aws\Common\Aws;
use Aws\Ec2\Ec2Client;
use Guzzle\Http\Message\Response;
use Guzzle\Plugin\Mock\MockPlugin;

/**
 * Provides some basics for mocking EC2 responses for testing instance strategies
 *
 * @package Sparky\Tests\InstanceStrategy
 */
class Ec2MockHelper
{
    /**
     * @var MockPlugin
     */
    protected $_mockPlugin;
    /**
     * @var Ec2Client
     */
    protected $_ec2;

    /**
     * Setup Ec2Client that can mock responses
     */
    public function __construct()
    {
        $this->_mockPlugin = new MockPlugin();
        $this->_ec2        = Aws::factory([
            'region'          => 'us-east-1',
            'client.backoff'  => FALSE,
            'request.options' => [
                'proxy' => '127.0.0.1:8888'
            ]
        ])->get('ec2');
        $this->_ec2->setSslVerification(FALSE);
        $this->_ec2->addSubscriber($this->_mockPlugin);
    }

    /**
     * @return Ec2Client
     */
    public function getEc2Client()
    {
        return $this->_ec2;
    }

    /**
     * @param int $status
     * @param string $responseData
     * @param array $replace
     */
    public function addResponse($status, $responseData = NULL, $replace = [])
    {
        $response = new Response($status);
        if(is_string($responseData))
        {
            if(substr($responseData, 0, 5) == '<?xml')
            {
                $response->setBody($responseData);
            } else
            {
                $response->setBody($this->_loadResponse($responseData, $replace));
            }
        }
        $this->_mockPlugin->addResponse($response);
    }

    /**
     * Loads a fake XML response for EC2
     *
     * @param string $file
     * @param array $replace
     * @return string
     */
    protected function _loadResponse($file, $replace = [])
    {
        $contents = file_get_contents($file);
        foreach ($replace as $key => $value)
        {
            $contents = str_replace('%' . $key . '%', $value, $contents);
        }
        return $contents;
    }
}
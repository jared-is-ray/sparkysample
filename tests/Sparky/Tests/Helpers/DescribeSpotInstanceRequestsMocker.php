<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Helpers;

class DescribeSpotInstanceRequestsMocker
{
    private $_responseWrapper = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<DescribeSpotInstanceRequestsResponse xmlns="http://ec2.amazonaws.com/doc/2014-10-01/">
    <requestId>2dbd9bf7-9ad9-49d7-8f54-283c441a6169</requestId>
    <spotInstanceRequestSet>
        %requests%
    </spotInstanceRequestSet>
</DescribeSpotInstanceRequestsResponse>
XML;
    private $_spotRequestXml = <<<XML
<item>
    <spotInstanceRequestId>%id%</spotInstanceRequestId>
    <spotPrice>0.200000</spotPrice>
    <type>one-time</type>
    <state>%state%</state>
    <status>
        <code>%status%</code>
        <updateTime>2015-03-23T23:13:18.000Z</updateTime>
        <message>Your Spot request is fulfilled.</message>
    </status>
    <launchSpecification>
        <imageId>ami-306b2958</imageId>
        <keyName>devs</keyName>
        <groupSet>
            <item>
                <groupId>sg-4c2ab528</groupId>
                <groupName>test-vpc-NatSG-SBL2M2WCUKPY</groupName>
            </item>
        </groupSet>
        <instanceType>c3.large</instanceType>
        <placement>
            <availabilityZone>us-east-1c</availabilityZone>
        </placement>
        <blockDeviceMapping>
            <item>
                <deviceName>/dev/sdf</deviceName>
                <virtualName>ephemeral0</virtualName>
            </item>
            <item>
                <deviceName>/dev/sdg</deviceName>
                <virtualName>ephemeral1</virtualName>
            </item>
        </blockDeviceMapping>
        <monitoring>
            <enabled>false</enabled>
        </monitoring>
        <subnetId>subnet-1a1a1a1a</subnetId>
        <iamInstanceProfile>
            <arn>arn:aws:iam::************:instance-profile/DevEc2</arn>
        </iamInstanceProfile>
    </launchSpecification>
    <instanceId>%instance-id%</instanceId>
    <createTime>2015-03-23T23:10:30.000Z</createTime>
    <productDescription>Linux/UNIX</productDescription>
    <tagSet>
        <item>
            <key>sparky:strategy:name</key>
            <value>dollar</value>
        </item>
        <item>
            <key>sparky:group:name</key>
            <value>test</value>
        </item>
        <item>
            <key>sparky:strategy:type</key>
            <value>OnDemand</value>
        </item>
    </tagSet>
    <launchedAvailabilityZone>us-east-1c</launchedAvailabilityZone>
</item>
XML;

    private $_requests = [];

    public static function make()
    {
        return new self();
    }

    /**
     * @param string $id
     * @param string $state
     * @param string $status
     * @param string $instanceId
     * @return DescribeSpotInstanceRequestsMocker
     */
    public function addSpotRequest($id, $state, $status, $instanceId = '')
    {
        $this->_requests[] = [
            'id'          => $id,
            'state'       => $state,
            'status'      => $status,
            'instance-id' => $instanceId
        ];
        return $this;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $xml = [];
        foreach($this->_requests as $instance)
        {
            $spotXml = $this->_spotRequestXml;
            foreach($instance as $k => $v)
            {
                $spotXml = str_replace('%' . $k . '%', $v, $spotXml);
            }
            $xml[] = $spotXml;
        }

        return str_replace('%requests%', implode("\n", $xml), $this->_responseWrapper);
    }
}
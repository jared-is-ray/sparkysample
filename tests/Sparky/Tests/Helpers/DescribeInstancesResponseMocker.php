<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Helpers;

class DescribeInstancesResponseMocker
{
    private $_responseWrapper = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<DescribeInstancesResponse xmlns="http://ec2.amazonaws.com/doc/2014-10-01/">
    <requestId>2a20ca79-ff64-464a-a741-0a231d217c67</requestId>
    <reservationSet>
        <item>
            <reservationId>r-caf4e42b</reservationId>
            <ownerId>************</ownerId>
            <groupSet>
                <item>
                    <groupId>sg-58991e32</groupId>
                    <groupName>dev-jray-DevSG-1LCFL21BTQ1U6</groupName>
                </item>
            </groupSet>
            <instancesSet>
                %instances%
            </instancesSet>
            <requesterId>226008221399</requesterId>
        </item>
    </reservationSet>
</DescribeInstancesResponse>
XML;
    private $_instanceXml = <<<XML
<item>
<instanceId>%instance-id%</instanceId>
<imageId>ami-306b2958</imageId>
<instanceState>
    <code>0</code>
    <name>%state-name%</name>
</instanceState>
<privateDnsName>ip-10-0-30-231.ec2.internal</privateDnsName>
<dnsName/>
<reason/>
<keyName>devs</keyName>
<amiLaunchIndex>0</amiLaunchIndex>
<productCodes/>
<instanceType>t2.micro</instanceType>
<launchTime>%launch-time%</launchTime>
<placement>
    <availabilityZone>us-east-1c</availabilityZone>
    <groupName/>
    <tenancy>default</tenancy>
</placement>
<monitoring>
    <state>disabled</state>
</monitoring>
<subnetId>subnet-1a1a1a1a</subnetId>
<vpcId>vpc-ff9cd69a</vpcId>
<privateIpAddress>10.0.30.231</privateIpAddress>
<sourceDestCheck>true</sourceDestCheck>
<groupSet>
    <item>
        <groupId>sg-1a1a1a1a</groupId>
        <groupName>stage-web-208-BalancerSG-1DQI52FHQV4NU</groupName>
    </item>
</groupSet>
<architecture>x86_64</architecture>
<rootDeviceType>ebs</rootDeviceType>
<rootDeviceName>/dev/xvda</rootDeviceName>
<blockDeviceMapping/>
<virtualizationType>hvm</virtualizationType>
<clientToken>0ea525d499a6ed5ea6d7abb33e4f72d342703ff3</clientToken>
<spotInstanceRequestId>%spot-request-id%</spotInstanceRequestId>
<hypervisor>xen</hypervisor>
<networkInterfaceSet>
    <item>
        <networkInterfaceId>eni-1a1a1a1a</networkInterfaceId>
        <subnetId>subnet-1a1a1a1a</subnetId>
        <vpcId>vpc-ff9cd69a</vpcId>
        <description/>
        <ownerId>************</ownerId>
        <status>in-use</status>
        <macAddress>0e:8f:5c:f6:ce:95</macAddress>
        <privateIpAddress>10.0.30.231</privateIpAddress>
        <privateDnsName>ip-10-0-30-231.ec2.internal</privateDnsName>
        <sourceDestCheck>true</sourceDestCheck>
        <groupSet>
            <item>
                <groupId>sg-1a1a1a1a</groupId>
                <groupName>stage-web-208-BalancerSG-1DQI52FHQV4NU</groupName>
            </item>
        </groupSet>
        <attachment>
            <attachmentId>eni-1a1a1a1a-c211e3bb</attachmentId>
            <deviceIndex>0</deviceIndex>
            <status>attaching</status>
            <attachTime>2015-03-16T23:15:05.000Z</attachTime>
            <deleteOnTermination>true</deleteOnTermination>
        </attachment>
        <privateIpAddressesSet>
            <item>
                <privateIpAddress>10.0.30.231</privateIpAddress>
                <privateDnsName>ip-10-0-30-231.ec2.internal</privateDnsName>
                <primary>true</primary>
            </item>
        </privateIpAddressesSet>
    </item>
</networkInterfaceSet>
<iamInstanceProfile>
    <arn>arn:aws:iam::************:instance-profile/DevEc2</arn>
    <id>******************</id>
</iamInstanceProfile>
<ebsOptimized>false</ebsOptimized>
</item>
XML;

    private $_emptyXml = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<DescribeInstancesResponse xmlns="http://ec2.amazonaws.com/doc/2014-10-01/">
    <requestId>dc976aac-d25c-4ba4-9166-87e3054e7f9e</requestId>
    <reservationSet></reservationSet>
</DescribeInstancesResponse>
XML;


    private $_instances = [];

    public static function make()
    {
        return new self();
    }

    /**
     * @param string $id
     * @param string $state
     * @param string $spotRequestId
     * @return DescribeInstancesResponseMocker
     */
    public function addInstance($id, $state, $spotRequestId = '')
    {
        $this->_instances[] = ['instance-id' => $id, 'state-name' => $state, 'spot-request-id' => $spotRequestId];
        return $this;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $xml = [];
        foreach($this->_instances as $instance)
        {
            $instanceXml = $this->_instanceXml;
            foreach($instance as $k => $v)
            {
                $instanceXml = str_replace('%' . $k . '%', $v, $instanceXml);
            }
            $xml[] = $instanceXml;
        }
        if(count($xml) > 0)
        {
            return str_replace('%instances%', implode("\n", $xml), $this->_responseWrapper);
        } else
        {
            return $this->_emptyXml;
        }
    }
}
<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Helpers;

class GetMetricStatisticsResponseMocker
{
    private $_responseWrapper = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<GetMetricStatisticsResponse xmlns="http://monitoring.amazonaws.com/doc/2010-08-01/">
    <GetMetricStatisticsResult>
        <Datapoints>
            %datapoints%
        </Datapoints>
        <Label>NetworkIn</Label>
    </GetMetricStatisticsResult>
    <ResponseMetadata>
        <RequestId>cd40ba14-fb3f-11e4-89f9-336d39185660</RequestId>
    </ResponseMetadata>
</GetMetricStatisticsResponse>
XML;
    private $_datapointXml = <<<DATAPOINT
<member>
    <Timestamp>2015-05-15T19:29:00Z</Timestamp>
    <SampleCount>1.0</SampleCount>
    <Unit>Bytes</Unit>
    <%stat%>%value%</%stat%>
</member>
DATAPOINT;

    private $_datapoints = [];

    public static function make()
    {
        return new self();
    }

    /**
     * @param string $stat
     * @param int|float $value
     * @return GetMetricStatisticsResponseMocker
     */
    public function addDatapoint($stat, $value)
    {
        $this->_datapoints[] = [
            'stat'        => $stat,
            'value'       => $value
        ];
        return $this;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        $xml = [];
        foreach($this->_datapoints as $instance)
        {
            $spotXml = $this->_datapointXml;
            foreach($instance as $k => $v)
            {
                $spotXml = str_replace('%' . $k . '%', $v, $spotXml);
            }
            $xml[] = $spotXml;
        }

        return str_replace('%datapoints%', implode("\n", $xml), $this->_responseWrapper);
    }
}
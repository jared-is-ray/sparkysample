<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\Actions;


use Sparky\Clock;
use Sparky\Instances\Instance;
use Sparky\Tests\Helpers\DescribeInstancesResponseMocker;
use Sparky\Tests\Helpers\Ec2MockHelper;
use Sparky\Tests\SparkyTestFactory;

class TerminateInstanceTest extends \PHPUnit_Framework_TestCase
{
    public function testTerminationSuccess()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());


        $testInstance = Instance::fromInfo([
            'InstanceId' => 'i-123',
            'State'      => ['Name' => 'running'],
            'LaunchTime' => '2015-05-22T03:00:00.000Z',
        ]);

        $action = $this->_getTestAction($factory, $testInstance);

        $mockEc2->addResponse(200);
        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'running')
            ->getXml()
        );
        $action->execute();
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'shutting-down')
            ->getXml()
        );
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'terminated')
            ->getXml()
        );
        self::assertFalse($action->isRunning());
        self::assertTrue($action->getResult()->isSuccess());
    }

    public function testTerminateError()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());

        $testInstance = Instance::fromInfo([
            'InstanceId' => 'i-123',
            'State'      => ['Name' => 'running'],
            'LaunchTime' => '2015-05-22T03:00:00.000Z'
        ]);

        $action = $this->_getTestAction($factory, $testInstance);

        $mockEc2->addResponse(400);
        $action->execute();
        self::assertFalse($action->isRunning());
        self::assertFalse($action->getResult()->isSuccess());
    }

    /**
     * @param SparkyTestFactory $factory
     * @param Instance $instance
     * @return \Sparky\Actions\ActionInterface
     */
    private function _getTestAction($factory, $instance)
    {
        return $factory->getActionInstance(
            'TerminateInstance',
            [
                'instance' => $instance
            ]
        );
    }
}

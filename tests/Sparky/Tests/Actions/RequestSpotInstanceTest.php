<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\Actions;


use Sparky\Clock;
use Sparky\Instances\InstanceConfig;
use Sparky\Instances\SpotInstanceRequest;
use Sparky\TagHelper;
use Sparky\Tests\Helpers\DescribeSpotInstanceRequestsMocker;
use Sparky\Tests\Helpers\Ec2MockHelper;
use Sparky\Tests\Instances\FakeInstanceConfig;
use Sparky\Tests\SparkyTestFactory;
use Sparky\Actions\RequestSpotInstance;

class RequestSpotInstanceTest extends \PHPUnit_Framework_TestCase
{
    public function testBid()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setEc2($mockEc2->getEc2Client());
        $factory->setClock($clock);
        $action = $this->_getTestAction($factory);

        $mockEc2->addResponse(200, __DIR__ . '/Responses/request-spot-instances-success.xml', [
            'spot-request-id' => 'sir-123'
        ]);
        // for bid tagging request
        $mockEc2->addResponse(200);
        $action->execute();

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'open', 'pending-evaluation')->getXml());
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'open', 'price-too-low')->getXml());
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'open', 'pending-fulfillment')->getXml());
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'active', 'fulfilled')->getXml());
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'active', 'fulfilled', 'i-123')->getXml());
        // for instance tagging request
        $mockEc2->addResponse(200);
        self::assertFalse($action->isRunning());

        $result = $action->getResult();
        self::assertTrue($result->isSuccess());
    }

    public function testBidFailure()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setEc2($mockEc2->getEc2Client());
        $factory->setClock($clock);
        $action = $this->_getTestAction($factory);

        $mockEc2->addResponse(200, __DIR__ . '/Responses/request-spot-instances-success.xml', [
            'spot-request-id' => 'sir-123'
        ]);
        // failure to tag
        $mockEc2->addResponse(400);
        $action->execute();

        // retry tagging succeeds
        $mockEc2->addResponse(200);
        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'open', 'pending-evaluation')->getXml());
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'cancelled', 'canceled-before-fulfillment')->getXml());

        self::assertFalse($action->isRunning());
        $result = $action->getResult();
        self::assertFalse($result->isSuccess());
    }

    public function testErrorRequestingSpot()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());
        $action = $this->_getTestAction($factory);

        $mockEc2->addResponse(400, __DIR__ . '/Responses/request-spot-instances-error.xml');
        $action->execute();

        self::assertFalse($action->isRunning());
        $result = $action->getResult();
        self::assertFalse($result->isSuccess());
    }

    public function testRestoredSpotInstanceRequest()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());

        $spotData = [
            'SpotInstanceRequestId' => 'sir-123',
            'SpotPrice' => 1.23,
            'Type' => 'one-time',
            'State' => 'open',
            'Status' => [
                'Code' => 'pending-fulfillment',
                'Message' => 'blah blah',
                'UpdateTime' => $clock->getXmlTimestamp()
            ],
            'CreateTime' => $clock->getXmlTimestamp()
        ];

        $action = $factory->getActionInstance(
            'RequestSpotInstance',
            [
                'spotRequest' => SpotInstanceRequest::fromInfo($spotData),
                'tags'        => new TagHelper()
            ]
        );

        $action->execute();

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
                ->addSpotRequest('sir-123', 'open', 'pending-evaluation')
                ->getXml());

        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
                ->addSpotRequest('sir-123', 'active', 'fulfilled', 'i-1234')->getXml());
        // fail instance tagging request
        $mockEc2->addResponse(400);
        // ensure action is not finished until instance is tagged
        self::assertTrue($action->isRunning());

        // succeed instance tagging request
        $mockEc2->addResponse(200);
        self::assertFalse($action->isRunning());
        self::assertTrue($action->getResult()->isSuccess());
    }

    public function testCancelledButFulfilled()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setEc2($mockEc2->getEc2Client());
        $factory->setClock($clock);
        $action = $this->_getTestAction($factory);

        $mockEc2->addResponse(200, __DIR__ . '/Responses/request-spot-instances-success.xml', [
            'spot-request-id' => 'sir-123'
        ]);
        // for tagging request
        $mockEc2->addResponse(200);
        $action->execute();

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'open', 'pending-fulfillment')->getXml());
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'cancelled', 'request-canceled-and-instance-running')->getXml());
        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'cancelled', 'request-canceled-and-instance-running', 'i-123')->getXml());
        // succeed instance tagging request
        $mockEc2->addResponse(200);
        self::assertFalse($action->isRunning());

        $result = $action->getResult();
        self::assertEquals('i-123', $result->getInstanceId());
        self::assertTrue($result->isSuccess());
    }


    /**
     * @param SparkyTestFactory $factory
     * @return \Sparky\Actions\ActionInterface
     */
    private function _getTestAction($factory)
    {
        return $factory->getActionInstance(
            'RequestSpotInstance',
            [
                'price'          => 1,
                'instanceConfig' => new FakeInstanceConfig(),
                'tags'           => new TagHelper(['test' => 'tag']),
                'timeout'        => 120
            ]
        );
    }
}

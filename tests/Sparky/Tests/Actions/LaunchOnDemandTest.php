<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\Actions;

use Sparky\Clock;
use Sparky\Instances\InstanceConfig;
use Sparky\TagHelper;
use Sparky\Tests\Helpers\Ec2MockHelper;
use Sparky\Tests\Instances\FakeInstanceConfig;
use Sparky\Tests\SparkyTestFactory;

class LaunchOnDemandTest extends \PHPUnit_Framework_TestCase
{
    public function testLaunch()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());
        $action = $this->_getTestAction($factory);

        $mockEc2->addResponse(200, __DIR__ . '/Responses/run-instances-success.xml');
        // for tagging call
        $mockEc2->addResponse(200);
        $action->execute();

        // assert action is in "running" state until instance goes into "running"
        $mockEc2->addResponse(200, __DIR__ . '/Responses/describe-instances-success.xml', [
            'state-name' => 'pending',
            'launch-time' => $clock->getXmlTimestamp()
        ]);

        self::assertTrue($action->isRunning());

        $mockEc2->addResponse(200, __DIR__ . '/Responses/describe-instances-success.xml', [
            'state-name' => 'running',
            'launch-time' => $clock->getXmlTimestamp()
        ]);
        self::assertFalse($action->isRunning());
        $result = $action->getResult();
        self::assertTrue($result->isSuccess());
    }

    public function testLaunchError()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());
        $action = $this->_getTestAction($factory);

        $mockEc2->addResponse(400, __DIR__ . '/Responses/run-instances-error.xml');
        $action->execute();

        self::assertFalse($action->isRunning());
        $result = $action->getResult();
        self::assertFalse($result->isSuccess());
    }

    /**
     * @param SparkyTestFactory $factory
     * @return \Sparky\Actions\ActionInterface
     */
    private function _getTestAction($factory)
    {
        return $factory->getActionInstance(
            'LaunchOnDemand',
            [
                'instanceConfig' => new FakeInstanceConfig(),
                'tags'           => new TagHelper(['some' => 'tag'])
            ]
        );
    }
}

<?php
namespace Sparky\Tests\Actions;

use Sparky\Actions\ActionInterface;
use Sparky\Actions\ActionResult;

class FakeAction implements ActionInterface
{
    /**
     * @var ActionResult
     */
    private $_result;
    
    /**
     * @var bool
     */
    private $_isRunning = TRUE;

    public function __construct()
    {
        $this->_result = new ActionResult();
    }

    /**
     * @return boolean
     */
    public function isRunning()
    {
        return $this->_isRunning;
    }

    /**
     * @param bool $isSuccess
     */
    public function setResultSuccess($isSuccess, $instanceId = NULL)
    {
        $this->_isRunning = FALSE;
        $this->_result->setIsSuccess($isSuccess);
        if($instanceId !== NULL)
        {
            $this->_result->setInstanceId($instanceId);
        }
    }

    public function execute()
    {
        return $this;
    }

    public function getResult()
    {
        return $this->_result;
    }
}
<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\Instances;


use Sparky\Clock;
use Sparky\Instances\InstanceContext;
use Sparky\InstanceStrategies\Events\Events;
use Sparky\InstanceStrategies\Events\InstanceFound;
use Sparky\InstanceStrategies\Events\InstanceLaunchSucceeded;
use Sparky\Tests\Helpers\DescribeSpotInstanceRequestsMocker;
use Sparky\Tests\Helpers\Ec2MockHelper;
use Sparky\Tests\Helpers\DescribeInstancesResponseMocker;
use Sparky\Tests\SparkyTestFactory;
use Symfony\Component\EventDispatcher\EventDispatcher;

class InstanceContextTest extends \PHPUnit_Framework_TestCase
{
    public function testIt()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());

        $ed = new EventDispatcher();
        $instanceContext = new InstanceContext([
            'factory'         => $factory,
            'eventDispatcher' => $ed
        ]);

        self::assertEquals(0, $instanceContext->getCountRunning());

        $ed->dispatch(Events::LAUNCH_STARTED);
        self::assertEquals(1, $instanceContext->getCountLaunching());

        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'running', 'sir-123')->getXml());

        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'open', 'pending-evaluation', 'i-123')->getXml());

        $ed->dispatch(Events::LAUNCH_SUCCEEDED, new InstanceLaunchSucceeded('i-123'));

        $instanceContext->rewind();
        self::assertEquals('sir-123', $instanceContext->current()->getSpotBid()->getId());

        self::assertEquals(0, $instanceContext->getCountLaunching());
        self::assertEquals(1, $instanceContext->getCountRunning());

        $ed->dispatch(Events::LAUNCH_STARTED);
        $ed->dispatch(Events::LAUNCH_STARTED);
        self::assertEquals(2, $instanceContext->getCountLaunching());

        $ed->dispatch(Events::LAUNCH_FAILED);
        self::assertEquals(1, $instanceContext->getCountLaunching());

        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-345', 'running')->getXml());
        $ed->dispatch(Events::INSTANCE_FOUND, new InstanceFound('i-345'));
        self::assertEquals(2, $instanceContext->getCountRunning());

        // simulate one instance shutting dow
        $instanceResponse = DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'running')
            ->addInstance('i-345', 'shutting-down');


        $mockEc2->addResponse(200, $instanceResponse->getXml());

        // refresh interval not yet reached
        self::assertEquals(2, $instanceContext->getCountRunning());

        // refresh interval reached
        $clock->addSeconds(31);
        self::assertEquals(1, $instanceContext->getCountRunning());
    }

    public function testPendingInstanceStaysRegistered()
    {
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());

        $ed = new EventDispatcher();
        $instanceContext = new InstanceContext([
            'factory'         => $factory,
            'eventDispatcher' => $ed
        ]);

        $refreshInterval = 30;

        self::assertEquals(0, $instanceContext->getCountRunning());

        $ed->dispatch(Events::LAUNCH_STARTED);

        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'pending')->getXml());

        $ed->dispatch(Events::LAUNCH_SUCCEEDED, new InstanceLaunchSucceeded('i-123'));

        $clock->addSeconds($refreshInterval + 1);
        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'pending')->getXml());
        self::assertEquals(1, $instanceContext->getCountRunning());
        self::assertEquals(0, $instanceContext->getCountLaunching());
    }
}

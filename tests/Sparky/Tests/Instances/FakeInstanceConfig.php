<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\Instances;

use Sparky\Instances\InstanceConfig;

class FakeInstanceConfig extends InstanceConfig
{
    public static $defaultConfig = [
        'ami'                 => 'ami-306b2958',
        'securityGroupIds'    => ['sg-4c2ab528'],
        'iamProfileArn'       => 'arn:aws:iam::************:instance-profile/DevEc2',
        'keyName'             => 'devs',
        'userData'            => 'S3=BaseGroup',
        'instanceType'        => 't2.micro',
        'subnetId'            => 'subnet-1a1a1a1a',
        'availabilityZone'    => 'us-east-1c',
        'blockDeviceMappings' => [
            [
                'VirtualName' => 'ephemeral0',
                'DeviceName'  => '/dev/sdf'
            ],
            [
                'VirtualName' => 'ephemeral1',
                'DeviceName'  => '/dev/sdg'
            ]
        ],
        'monitoring'          => FALSE,
    ];

    public function __construct()
    {
        parent::__construct(self::$defaultConfig);
    }
}
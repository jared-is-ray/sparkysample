<?php
namespace Sparky\Tests\Instances;

use Sparky\Instances\Instance;
use Sparky\Instances\InstanceContext;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class FakeInstanceContext extends InstanceContext
{
    protected function _refreshInstances()
    {
        // do nothing
    }

    /**
     * @param Instance $instance
     */
    public function addInstance($instance)
    {
        $this->_instances[$instance->getId()] = $instance;
    }

    protected function _addInstance($instanceId)
    {
        if(!isset($this->_instances[$instanceId]))
        {
            $this->_instances[$instanceId] = Instance::fromInfo([
                'InstanceId' => $instanceId,
                'State' => [
                    'Name' => 'running'
                ],
                'LaunchTime' => '2015-05-22T03:00:00.000Z'
            ]);
            if (in_array($this->_instances[$instanceId]->getState(), self::$_runningStates))
            {
                $this->_countRunning++;
            }
        }
    }
}
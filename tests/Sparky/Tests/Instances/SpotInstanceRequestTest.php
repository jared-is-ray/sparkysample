<?php

namespace Sparky\Tests\Instances;

use Sparky\Instances\SpotInstanceRequest;

class SpotInstanceRequestTest extends \PHPUnit_Framework_TestCase
{
    public function testIt()
    {
        $sir = new SpotInstanceRequest();

        $sir->populate(
            [
                'SpotInstanceRequestId'    => 'sir-02f2j4bx',
                'SpotPrice'                => '0.140000',
                'Type'                     => 'one-time',
                'State'                    => 'active',
                'Status'                   =>
                    [
                        'Code'       => 'fulfilled',
                        'UpdateTime' => '2015-03-04T13:08:03.000Z',
                        'Message'    => 'Your Spot request is fulfilled.',
                    ],
                'InstanceId'               => 'i-a1a1a1a1',
                'CreateTime'               => '2015-03-04T13:04:43.000Z',
            ]);

        // try updating w/ different id
        try
        {
            $sir->populate(['SpotInstanceRequestId' => 'sir-02f2j4bsx']);
            self::fail('SpotInstanceRequest did not throw exception on invalid populate() call');
        } catch(\DomainException $e) {}
    }
}
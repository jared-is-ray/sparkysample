<?php

namespace Sparky\Tests\Instances;

use Sparky\Instances\Instance;

class InstanceTest extends \PHPUnit_Framework_TestCase
{
    public function testIt()
    {
        $instance = new Instance();

        $instance->populate(
            [
                'InstanceId'            => 'i-109937e0',
                'State'                 =>
                    [
                        'Name' => 'running'
                    ],
                'LaunchTime' => '2015-05-22T03:00:00.000Z'
            ]
        );

        // try updating w/ different id
        try
        {
            $instance->populate(['InstanceId' => 'i-someOther']);
            self::fail('Instance did not throw exception on invalid populate() call');
        } catch (\DomainException $e)
        {
        }
    }
}
<?php

namespace Sparky\Tests;

use Sparky\Clock;
use Sparky\Config\Events\ConfigurationUpdated;
use Sparky\GroupConfig;
use Sparky\InstanceGroup;
use Sparky\SparkyFactory;
use Sparky\Tests\Instances\FakeInstanceConfig;
use Sparky\Tests\InstanceStrategies\FakeStrategy;
use Sparky\Tests\Instances\FakeInstanceContext;
use Sparky\Tests\KillStrategies\FakeKillStrategy;
use Symfony\Component\EventDispatcher\EventDispatcher;

class InstanceGroupTestCase extends \PHPUnit_Framework_TestCase
{
    public function testStrategies()
    {
        $clock = new Clock(Clock::MODE_TEST);
        $eventDispatcher = new EventDispatcher();
        $factory = new SparkyTestFactory();
        $factory->setEventDispatcher($eventDispatcher);
        $factory->setClock($clock);

        $instanceCtx = new FakeInstanceContext([
            'eventDispatcher' => $eventDispatcher,
            'factory' => $factory
        ]);
        $factory->addInstanceContext($instanceCtx);

        $strategy1 = new FakeStrategy(['eventDispatcher' => $eventDispatcher]);
        $strategy2 = new FakeStrategy(['eventDispatcher' => $eventDispatcher]);

        $factory->addStrategy($strategy1);
        $factory->addStrategy($strategy2);

        $scaler  = new TestScaler();
        $factory->addScaler($scaler);

        $killStrategy = new FakeKillStrategy(['eventDispatcher' => $eventDispatcher]);
        $factory->addKillStrategy($killStrategy);

        $group = new InstanceGroup('group1', $this->_makeGroupConfig($factory));
        $group->refresh();

        // 1 should launch because of group min
        self::assertEquals(1, $instanceCtx->getCountLaunching());
        self::assertEquals(0, $instanceCtx->getCountRunning());
        self::assertEquals(1, $strategy1->getLaunchedCount());
        self::assertEquals(0, $strategy2->getLaunchedCount());

        $scaler->setCapacity(2);
        $strategy1->failLaunch();
        $group->refresh();

        // 1 more should launch because of scale
        self::assertEquals(2, $instanceCtx->getCountLaunching());
        self::assertEquals(0, $instanceCtx->getCountRunning());
        self::assertEquals(3, $strategy1->getLaunchedCount());
        self::assertEquals(0, $strategy2->getLaunchedCount());

        $strategy1->setIsAvailable(FALSE);
        $scaler->setCapacity(5);
        $strategy1->succeedLaunch('i-12345');
        $group->refresh();

        // 1 should launch in 2nd strategy because first is unavailable
        // max of 1 more should launch because of group max
        self::assertEquals(2, $instanceCtx->getCountLaunching());
        self::assertEquals(1, $instanceCtx->getCountRunning());
        self::assertEquals(3, $strategy1->getLaunchedCount());
        self::assertEquals(1, $strategy2->getLaunchedCount());


        // TEST KILLS
        $scaler->setCapacity(0);
        $group->refresh();
        
        self::assertEquals(2, $instanceCtx->getCountTerminating());
        self::assertEquals(2, $instanceCtx->getCountLaunching());
        self::assertEquals(1, $instanceCtx->getCountRunning());
        self::assertEquals(3, $strategy1->getLaunchedCount());
        self::assertEquals(1, $strategy2->getLaunchedCount());

    }

    public function testConfigUpdates()
    {
        $clock = new Clock(Clock::MODE_TEST);
        $eventDispatcher = new EventDispatcher();
        $factory = new SparkyTestFactory();
        $factory->setEventDispatcher($eventDispatcher);
        $factory->setClock($clock);

        $instanceCtx1 = new FakeInstanceContext([
            'eventDispatcher' => $eventDispatcher,
            'factory' => $factory
        ]);
        $factory->addInstanceContext($instanceCtx1);

        $scaler1 = new TestScaler();
        $factory->addScaler($scaler1);

        $strategy1 = new FakeStrategy(['eventDispatcher' => $eventDispatcher]);
        $strategy2 = new FakeStrategy(['eventDispatcher' => $eventDispatcher]);

        $factory->addStrategy($strategy1);
        $factory->addStrategy($strategy2);

        $killStrategy1 = new FakeKillStrategy(['eventDispatcher' => $eventDispatcher]);
        $factory->addKillStrategy($killStrategy1);

        $conf = $this->_makeGroupConfig($factory);
        $group = new InstanceGroup('group1', $conf);
        $group->refresh();

        $newStrategy1 = new FakeStrategy(['eventDispatcher' => $eventDispatcher]);
        $newStrategy2 = new FakeStrategy(['eventDispatcher' => $eventDispatcher]);
        $factory->addStrategy($newStrategy1);
        $factory->addStrategy($newStrategy2);

        $scaler2  = new TestScaler();
        $factory->addScaler($scaler2);

        $killStrategy2 = new FakeKillStrategy(['eventDispatcher' => $eventDispatcher]);
        $factory->addKillStrategy($killStrategy2);

        $instanceCtx2 = new FakeInstanceContext([
            'eventDispatcher' => $eventDispatcher,
            'factory' => $factory
        ]);
        $factory->addInstanceContext($instanceCtx2);

        $group->onConfigurationUpdated(new ConfigurationUpdated($conf));

        self::assertTrue($scaler1->isShutdown());
        self::assertTrue($strategy1->isShutdown());
        self::assertTrue($strategy2->isShutdown());
        self::assertTrue($killStrategy1->isShutdown());

        // new instances
        self::assertFalse($scaler2->isShutdown());
        self::assertFalse($newStrategy1->isShutdown());
        self::assertFalse($killStrategy2->isShutdown());

        $scaler2->setCapacity(3);
        $group->refresh();
        self::assertEquals(3, $newStrategy1->getLaunchedCount());
        self::assertEquals(0, $instanceCtx2->getCountRunning());
        self::assertEquals(3, $instanceCtx2->getCountLaunching());
        self::assertEquals(0, $instanceCtx2->getCountTerminating());
    }

    /**
     * @param SparkyFactory $factory
     * @return GroupConfig
     */
    private function _makeGroupConfig($factory)
    {
        return new GroupConfig([
            'scaler'       => 'TestScaler',
            'killStrategy' => 'MostExpensiveFirst',
            'min'          => 1,
            'max'          => 3,
            'instances'    => FakeInstanceConfig::$defaultConfig,
            'strategies'   => [
                'Strategy1' => ['type' => 'SomeStrategy', 'config' => []],
                'Strategy2' => ['type' => 'SomeStrategy', 'config' => []]
            ]
        ], $factory);
    }
}
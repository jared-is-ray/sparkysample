<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\KillStrategies;

use Sparky\Clock;
use Sparky\Instances\Instance;
use Sparky\Instances\SpotInstanceRequest;
use Sparky\KillStrategies\KillStrategyInterface;
use Sparky\SparkyFactory;
use Sparky\Tests\Actions\FakeAction;
use Sparky\Tests\Helpers\Ec2MockHelper;
use Sparky\Tests\Instances\FakeInstanceContext;
use Sparky\Tests\SparkyTestFactory;
use Symfony\Component\EventDispatcher\EventDispatcher;

class MostExpensiveFirstTest extends \PHPUnit_Framework_TestCase
{
    public function testIt()
    {
        $eventDispatcher = new EventDispatcher();
        $clock = new Clock(Clock::MODE_TEST);
        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEventDispatcher($eventDispatcher);

        $mockEc2 = new Ec2MockHelper();
        $factory->setEc2($mockEc2->getEc2Client());

        $killAction1 = new FakeAction();
        $killAction2 = new FakeAction();
        $killAction3 = new FakeAction();
        $factory->addAction($killAction1);
        $factory->addAction($killAction2);
        $factory->addAction($killAction3);

        $context = new FakeInstanceContext([
            'factory'         => $factory,
            'eventDispatcher' => $eventDispatcher
        ]);

        $instance1 = Instance::fromInfo([
            'InstanceId' => 'i-123',
            'State' => ['Name' => 'running'],
            'LaunchTime' => '2015-05-22T03:00:00.000Z'
        ]);

        $instance2 = Instance::fromInfo([
            'InstanceId' => 'i-234',
            'State' => ['Name' => 'running'],
            'LaunchTime' => '2015-05-22T03:30:08.000Z'
        ]);

        $instance3 = Instance::fromInfo([
            'InstanceId' => 'i-345',
            'State' => ['Name' => 'running'],
            'LaunchTime' => '2015-05-22T03:30:08.000Z'
        ]);

        $instance2->setSpotBid(SpotInstanceRequest::fromInfo([
            'SpotInstanceRequestId' => 'sir-123',
            'SpotPrice' => '.004',
            'Type' => 'one-time',
            'State' => 'active',
            'Status' => [
                'Code' => 'fulfilled',
                'UpdateTime' => 's',
                'Message' => 'Some status message'
            ],
            'InstanceId' => 'i-123',
            'CreateTime' => 's'
        ]));

        $context->addInstance($instance1);
        $context->addInstance($instance2);
        $context->addInstance($instance3);

        self::assertEquals(0, $context->getCountTerminating());

        $kill = $this->_getKillStrategy($factory, [
            'instanceContext' => $context,
            'eventDispatcher' => $eventDispatcher
        ]);

        // simulate both instance are not past minimum run time
        $clock->setCurrentTime($clock->parseXmlTimestamp('2015-05-22T03:35:00.000Z'));
        $kill->refresh();
        $kill->killInstance();
        self::assertEquals(0, $context->getCountTerminating());

        // simulate one instance past minimum run time
        $clock->setCurrentTime($clock->parseXmlTimestamp('2015-05-22T04:00:00.000Z'));
        $kill->refresh();
        $kill->killInstance();
        $kill->killInstance();
        self::assertEquals(1, $context->getCountTerminating());

        // simulate others instances past minimum run time
        $clock->setCurrentTime($clock->parseXmlTimestamp('2015-05-22T04:30:00.000Z'));
        $kill->refresh();
        $kill->killInstance();
        $kill->killInstance();
        $kill->killInstance();
        self::assertEquals(3, $context->getCountTerminating());

        $kill->refresh();
        $killAction1->setResultSuccess(TRUE);
        $kill->refresh();
        self::assertEquals(2, $context->getCountTerminating());
        $killAction2->setResultSuccess(FALSE);
        $killAction3->setResultSuccess(TRUE);
        $kill->refresh();
        self::assertEquals(0, $context->getCountTerminating());
    }

    /**
     * @param SparkyFactory $factory
     * @param array $config
     * @return KillStrategyInterface
     */
    private function _getKillStrategy($factory, $config)
    {
        return $factory->getKillStrategyInstance(
            'MostExpensiveFirst',
            $config
        );
    }
}

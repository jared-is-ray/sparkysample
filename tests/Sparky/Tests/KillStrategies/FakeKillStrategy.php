<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\KillStrategies;


use Sparky\InstanceStrategies\Events\Events;
use Sparky\KillStrategies\KillStrategyInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FakeKillStrategy implements KillStrategyInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $_eventDispatcher;

    /**
     * @var bool
     */
    private $_shutdown = FALSE;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->_eventDispatcher = $config['eventDispatcher'];
    }

    public function init()
    {
        $this->_checkShutdown();
    }

    public function refresh()
    {
        $this->_checkShutdown();
    }

    /**
     * @return boolean
     */
    public function isShutdown()
    {
        return $this->_shutdown;
    }

    public function shutdown()
    {
        $this->_checkShutdown();
        $this->_shutdown = TRUE;
    }

    public function killInstance()
    {
        $this->_checkShutdown();
        $this->_eventDispatcher->dispatch(Events::TERMINATION_STARTED);
    }

    /**
     * Simulate a failed instance termination
     */
    public function failTermination()
    {
        $this->_eventDispatcher->dispatch(Events::TERMINATION_FAILED);
    }

    /**
     * Simulate a successful instance termination
     */
    public function succeedTermination()
    {
        $this->_eventDispatcher->dispatch(Events::TERMINATION_SUCCEEDED);
    }

    private function _checkShutdown()
    {
        if($this->_shutdown)
        {
            throw new \LogicException(sprintf('%s already shutdown', __CLASS__));
        }
    }
}
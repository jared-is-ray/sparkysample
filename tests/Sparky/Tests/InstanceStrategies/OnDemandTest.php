<?php
/**
 * 
 * @package
 * @subpackage
 * 
 * @author     jray
 */

namespace Sparky\Tests\InstanceStrategies;

use Sparky\Clock;
use Sparky\InstanceStrategies\OnDemand;
use Sparky\SparkyFactory;
use Sparky\Tests\Actions\FakeAction;
use Sparky\Tests\Helpers\DescribeInstancesResponseMocker;
use Sparky\Tests\Helpers\Ec2MockHelper;
use Sparky\Tests\Instances\FakeInstanceConfig;
use Sparky\Tests\Instances\FakeInstanceContext;
use Sparky\Tests\SparkyTestFactory;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OnDemandTest extends \PHPUnit_Framework_TestCase
{
    public function testFlow()
    {
        $eventDispatcher = new EventDispatcher();
        $clock = new Clock(Clock::MODE_TEST);
        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEventDispatcher($eventDispatcher);
        $mockEc2 = new Ec2MockHelper();
        $factory->setEc2($mockEc2->getEc2Client());
        $context = new FakeInstanceContext([
            'factory' => $factory,
            'eventDispatcher' => $eventDispatcher
        ]);


        $action1 = new FakeAction();
        $action2 = new FakeAction();

        $factory->addAction($action1);
        $factory->addAction($action2);

        $strategy = $this->_getTestStrategy($factory, $eventDispatcher);
        // for findInstances call
        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()->getXml());
        $strategy->init();

        self::assertEquals(0, $context->getCountLaunching());
        self::assertTrue($strategy->isAvailable());

        $strategy->launchInstance();
        self::assertEquals(1, $context->getCountLaunching());

        $strategy->launchInstance();
        self::assertEquals(2, $context->getCountLaunching());

        $action2->setResultSuccess(FALSE);
        $clock->addSeconds(31);
        $strategy->isAvailable();
        self::assertEquals(1, $context->getCountLaunching());

        $action1->setResultSuccess(TRUE, 'i-123');
        $clock->addSeconds(31);
        $strategy->isAvailable();
        self::assertEquals(0, $context->getCountLaunching());
    }

    public function testFindInstances()
    {
        $eventDispatcher = new EventDispatcher();
        $factory = new SparkyTestFactory();
        $factory->setEventDispatcher($eventDispatcher);

        $mockEc2 = new Ec2MockHelper();
        $factory->setEc2($mockEc2->getEc2Client());

        $context = new FakeInstanceContext([
            'factory' => $factory,
            'eventDispatcher' => $eventDispatcher
        ]);

        $strategy = $this->_getTestStrategy($factory, $eventDispatcher);

        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()
            ->addInstance('i-123', 'running')
            ->addInstance('i-234', 'pending')
            ->getXml()
        );

        $strategy->init();

        self::assertEquals(2, $context->getCountRunning());
    }

    /**
     * @param SparkyFactory $factory
     * @param EventDispatcherInterface $eventDispatcher
     * @return OnDemand
     */
    private function _getTestStrategy($factory, $eventDispatcher)
    {
        return new OnDemand([
            'factory'         => $factory,
            'instanceConfig'  => new FakeInstanceConfig(),
            'eventDispatcher' => $eventDispatcher,
            'name'            => 'testStrategy',
            'groupName'       => 'group1',
        ]);
    }
}

<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\InstanceStrategies;

use Sparky\InstanceStrategies\Events\Events;
use Sparky\InstanceStrategies\Events\InstanceLaunchSucceeded;
use Sparky\InstanceStrategies\InstanceStrategyInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class FakeStrategy implements InstanceStrategyInterface
{
    /**
     * @var bool
     */
    private $_isAvailable = TRUE;

    /**
     * @var int
     */
    private $_launchedCount = 0;
    
    /**
     * @var EventDispatcherInterface
     */
    private $_eventDispatcher;

    /**
     * @var bool
     */
    private $_shutdown = FALSE;

    /**
     * @var string
     */
    private $_name;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->_name = uniqid('strategy');
        $this->_eventDispatcher = $config['eventDispatcher'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        $this->_checkShutdown();
        return $this->_name;
    }

    /**
     * @return void
     */
    public function init()
    {
        $this->_checkShutdown();
    }

    /**
     * @return boolean
     */
    public function isAvailable()
    {
        $this->_checkShutdown();
        return $this->_isAvailable;
    }

    /**
     * @param boolean $isAvailable
     */
    public function setIsAvailable($isAvailable)
    {
        $this->_isAvailable = $isAvailable;
    }

    /**
     * @return int
     */
    public function getLaunchedCount()
    {
        return $this->_launchedCount;
    }

    /**
     * @return void
     */
    public function launchInstance()
    {
        $this->_checkShutdown();
        $this->_launchedCount++;
        $this->_eventDispatcher->dispatch(Events::LAUNCH_STARTED);
    }

    /**
     * Perform any cleanup necessary for before disposing of this strategy object
     *
     * @return void
     */
    public function shutdown()
    {
        $this->_checkShutdown();
        $this->_shutdown = TRUE;
    }

    /**
     * @return bool
     */
    public function isShutdown()
    {
        return $this->_shutdown;
    }

    private function _checkShutdown()
    {
        if($this->_shutdown)
        {
            throw new \LogicException('%s: strategy already shutdown', __CLASS__);
        }
    }

    /**
     * Simulate failed launch
     */
    public function failLaunch()
    {
        $this->_eventDispatcher->dispatch(Events::LAUNCH_FAILED);
    }

    /**
     * Simulate successful launch with given instance id
     * @param string $instanceId
     */
    public function succeedLaunch($instanceId)
    {
        $this->_eventDispatcher->dispatch(
            Events::LAUNCH_SUCCEEDED,
            new InstanceLaunchSucceeded($instanceId)
        );
    }
}
<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests\InstanceStrategies;


use Sparky\Clock;
use Sparky\InstanceStrategies\SimpleSpotBid;
use Sparky\Tests\Actions\FakeAction;
use Sparky\Tests\Helpers\DescribeInstancesResponseMocker;
use Sparky\Tests\Helpers\DescribeSpotInstanceRequestsMocker;
use Sparky\Tests\Helpers\Ec2MockHelper;
use Sparky\Tests\Instances\FakeInstanceConfig;
use Sparky\Tests\Instances\FakeInstanceContext;
use Sparky\Tests\SparkyTestFactory;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SimpleSpotBidTest extends \PHPUnit_Framework_TestCase
{
    public function testFlow()
    {
        $eventDispatcher = new EventDispatcher();
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());
        $factory->setEventDispatcher($eventDispatcher);

        $context = new FakeInstanceContext([
            'factory' => $factory,
            'eventDispatcher' => $eventDispatcher
        ]);

        $action1 = new FakeAction();
        $action2 = new FakeAction();

        $factory->addAction($action1);
        $factory->addAction($action2);

        $strategy = $this->_getTestStrategy($factory, $eventDispatcher);
        $mockEc2->addResponse(200, DescribeInstancesResponseMocker::make()->getXml());
        $strategy->init();

        $mockEc2->addResponse(
            200,
            __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml',
            [
                'spot-price' => .01
            ]
        );

        self::assertEquals(0, $context->getCountLaunching());
        self::assertEquals(0, $context->getCountRunning());
        self::assertTrue($strategy->isAvailable());
        // call again to ensure api is not called twice within refresh interval
        self::assertTrue($strategy->isAvailable());

        $strategy->launchInstance();
        self::assertEquals(1, $context->getCountLaunching());
        self::assertEquals(0, $context->getCountRunning());

        $action1->setResultSuccess(TRUE, 'i-1234');

        $mockEc2->addResponse(
            200,
            __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml',
            [
                'spot-price' => .01
            ]
        );
        // simulate 30 seconds to force action result to update
        $clock->addSeconds(31);
        $strategy->isAvailable();
        self::assertEquals(0, $context->getCountLaunching());
        self::assertEquals(1, $context->getCountRunning());

        // simulate 30 seconds to force bid info to update
        $clock->addSeconds(31);

        $mockEc2->addResponse(
            200,
            __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml',
            [
                'spot-price' => 1.304
            ]
        );
        self::assertFalse($strategy->isAvailable());

        try
        {
            $strategy->launchInstance();
            self::fail('SimpleSpotBid failed to throw exception in unavailable state');
        } catch(\LogicException $e) {}
    }

    public function testFindInstances()
    {
        $eventDispatcher = new EventDispatcher();
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());
        $factory->setEventDispatcher($eventDispatcher);

        $context = new FakeInstanceContext([
            'factory' => $factory,
            'eventDispatcher' => $eventDispatcher
        ]);

        $action1 = new FakeAction();
        $factory->addAction($action1);

        $strategy = $this->_getTestStrategy($factory, $eventDispatcher);
        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()
            ->addSpotRequest('sir-123', 'closed', 'system-error')
            ->addSpotRequest('sir-234', 'active', 'fulfilled', 'i-1234')
            ->addSpotRequest('sir-345', 'cancelled', 'request-canceled-and-instance-running', 'i-3456')
            ->addSpotRequest('sir-456', 'open', 'pending-evaluation')
            ->getXml());
        $strategy->init();

        self::assertEquals($context->getCountRunning(), 2);
        self::assertEquals($context->getCountLaunching(), 1);

        $action1->setResultSuccess(TRUE, 'i-2345');
        $mockEc2->addResponse(200, __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml');
        $strategy->isAvailable();
        self::assertEquals($context->getCountRunning(), 3);
        self::assertEquals($context->getCountLaunching(), 0);
    }

    public function testFailedLaunchDisables()
    {
        $eventDispatcher = new EventDispatcher();
        $mockEc2 = new Ec2MockHelper();
        $clock = new Clock(Clock::MODE_TEST);

        $factory = new SparkyTestFactory();
        $factory->setClock($clock);
        $factory->setEc2($mockEc2->getEc2Client());
        $factory->setEventDispatcher($eventDispatcher);

        $context = new FakeInstanceContext([
            'factory' => $factory,
            'eventDispatcher' => $eventDispatcher
        ]);

        $action1 = new FakeAction();
        $factory->addAction($action1);


        $strategy = $this->_getTestStrategy($factory, $eventDispatcher);
        $mockEc2->addResponse(200, DescribeSpotInstanceRequestsMocker::make()->getXml());
        $strategy->init();

        $mockEc2->addResponse(200, __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml');
        self::assertTrue($strategy->isAvailable());

        $strategy->launchInstance();
        $action1->setResultSuccess(FALSE);

        $clock->addSeconds(31);
        $mockEc2->addResponse(200, __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml');
        self::assertFalse($strategy->isAvailable());

        $clock->addSeconds(40);
        $mockEc2->addResponse(200, __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml');
        self::assertFalse($strategy->isAvailable());

        $clock->addSeconds(40);
        $mockEc2->addResponse(200, __DIR__ . '/Responses/describe-spot-price-history-zone-success.xml');
        self::assertTrue($strategy->isAvailable());
    }

    /**
     * @param \Sparky\SparkyFactory $factory
     * @param EventDispatcherInterface $eventDispatcher
     * @return SimpleSpotBid
     */
    private function _getTestStrategy($factory, $eventDispatcher)
    {
        return new SimpleSpotBid([
            'factory'         => $factory,
            'eventDispatcher' => $eventDispatcher,
            'price'           => 0.75,
            'name'            => 'testStrategy',
            'groupName'       => 'group2',
            'instanceConfig'  => new FakeInstanceConfig()
        ]);
    }
}

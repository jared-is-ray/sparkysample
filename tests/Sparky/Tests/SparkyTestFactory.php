<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests;


use Aws\CloudWatch\CloudWatchClient;
use Aws\Ec2\Ec2Client;
use Aws\S3\S3Client;
use Aws\Sqs\SqsClient;
use Sparky\Actions\ActionInterface;
use Sparky\Instances\InstanceContext;
use Sparky\InstanceStrategies\InstanceStrategyInterface;
use Sparky\KillStrategies\KillStrategyInterface;
use Sparky\Scalers\ScalerInterface;
use Sparky\SparkyFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SparkyTestFactory extends SparkyFactory
{
    /**
     * @var ScalerInterface[]
     */
    private $_scalers = [];

    /**
     * @var Ec2Client
     */
    private $_ec2;

    /**
     * @var \Sparky\Clock
     */
    private $_clock;

    /**
     * @var ActionInterface[]
     */
    private $_actions = [];

    /**
     * @var InstanceStrategyInterface[]
     */
    private $_strategies;

    /**
     * @var InstanceContext[]
     */
    private $_instanceContexts = [];
    
    /**
     * @var EventDispatcherInterface
     */
    private $_eventDispatcher;
    
    /**
     * @var KillStrategyInterface[]
     */
    private $_killStrategies = [];

    /**
     * @var CloudWatchClient
     */
    private $_cloudWatch;

    /**
     * @var SqsClient
     */
    private $_sqsClient;

    /**
     * @var S3Client
     */
    private $_s3;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher($eventDispatcher)
    {
        $this->_eventDispatcher = $eventDispatcher;
    }

    public function getEventDispatcherInstance()
    {
        return isset($this->_eventDispatcher) ? $this->_eventDispatcher : parent::getEventDispatcherInstance();
    }

    /**
     * @param ScalerInterface $scaler
     */
    public function addScaler($scaler)
    {
        $this->_scalers[] = $scaler;
    }

    public function getScalerInstance($type, $config)
    {
        if(count($this->_scalers) > 0)
        {
            return array_shift($this->_scalers);
        }
        return parent::getScalerInstance($type, $config);
    }

    /**
     * @param Ec2Client $ec2
     */
    public function setEc2($ec2)
    {
        $this->_ec2 = $ec2;
    }

    public function getEc2()
    {
        return isset($this->_ec2) ? $this->_ec2 : parent::getEc2();
    }

    /**
     * @param CloudWatchClient $cloudWatch
     */
    public function setCloudWatch($cloudWatch)
    {
        $this->_cloudWatch = $cloudWatch;
    }

    public function getCloudWatch()
    {
        return isset($this->_cloudWatch) ? $this->_cloudWatch : parent::getCloudWatch();
    }

    /**
     * @param SqsClient $sqsClient
     */
    public function setSqs($sqsClient)
    {
        $this->_sqsClient = $sqsClient;
    }

    public function getSqs()
    {
        return isset($this->_cloudWatch) ? $this->_cloudWatch : parent::getSqs();
    }

    /**
     * @param S3Client $s3
     */
    public function setS3($s3)
    {
        $this->_s3 = $s3;
    }

    public function getS3()
    {
        return isset($this->_s3) ? $this->_s3 : parent::getS3();
    }

    /**
     * @param \Sparky\Clock $clock
     */
    public function setClock($clock)
    {
        $this->_clock = $clock;
    }

    public function getClockInstance()
    {
        return isset($this->_clock) ? $this->_clock : parent::getClockInstance();
    }

    /**
     * Adds an Action object that the factory should return for the next call to getActionInstance().
     * For injecting test actions into the factory for testing.
     *
     * @param ActionInterface $action
     * @return void
     */
    public function addAction($action)
    {
        $this->_actions[] = $action;
    }

    public function getActionInstance($type, $config = [])
    {
        if(count($this->_actions) > 0)
        {
            return array_shift($this->_actions);
        }
        return parent::getActionInstance($type, $config);
    }

    /**
     * Adds an Action object that the factory should return for the next call to getStrategyInstance().
     * For injecting test strategies into the factory for testing.
     *
     * @param InstanceStrategyInterface $strategy
     */
    public function addStrategy($strategy)
    {
        $this->_strategies[] = $strategy;
    }

    public function getStrategyInstance($type, $config = [])
    {
        if(count($this->_strategies) > 0)
        {
            return array_shift($this->_strategies);
        }
        return parent::getStrategyInstance($type, $config);
    }

    /**
     * @param InstanceContext $instanceContext
     */
    public function addInstanceContext($instanceContext)
    {
        $this->_instanceContexts[] = $instanceContext;
    }

    public function getInstanceContext($type, $config)
    {
        if(count($this->_instanceContexts) > 0)
        {
            return array_shift($this->_instanceContexts);
        }
        return parent::getInstanceContext($type, $config);
    }

    /**
     * @param KillStrategyInterface $strategy
     */
    public function addKillStrategy($strategy)
    {
        $this->_killStrategies[] = $strategy;
    }

    public function getKillStrategyInstance($type, $config)
    {
        if(count($this->_killStrategies) > 0)
        {
            return array_shift($this->_killStrategies);
        }
        return parent::getKillStrategyInstance($type, $config);
    }
}
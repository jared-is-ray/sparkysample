<?php
/**
 * 
 * @package    
 * @subpackage 
 * 
 * @author     jray
 */

namespace Sparky\Tests;


use Sparky\Clock;
use Sparky\GroupConfig;
use Sparky\SparkyFactory;
use Sparky\Tests\Instances\FakeInstanceConfig;
use Sparky\Tests\Instances\FakeInstanceContext;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Noodlehaus\Config;
use Symfony\Component\EventDispatcher\EventDispatcher;

class GroupConfigTest extends \PHPUnit_Framework_TestCase
{
    public function testArrayConfig()
    {
        $factoryMock = $this->_getFactoryMock();

        $onDamand1Config = [
            'config1A'  => 123,
            'config1B'  => 456
        ];

        $onDamand2Config = [
            'config2A' => 'abc'
        ];

        $eventDispatcher = new EventDispatcher();

        $instanceContext = new FakeInstanceContext([
            'factory'         => $factoryMock,
            'eventDispatcher' => $eventDispatcher
        ]);

        $factoryMock->expects(self::exactly(2))
            ->method('getStrategyInstance')
            ->withConsecutive(
                [
                    'OnDemand',
                    self::isType('array')
                ],
                [
                    'OnDemand',
                    self::isType('array')
                ]
            );

        $factoryMock->expects(self::exactly(2))
            ->method('getInstanceConfig');

        $factoryMock->expects(self::exactly(1))
            ->method('getKillStrategyInstance')
            ->with('MostExpensiveFirst', ['eventDispatcher' => $eventDispatcher, 'instanceContext' => $instanceContext]);

        $config = [
            'name'          => 'some-group',
            'factory'       => $factoryMock,
            'scaler'        => 'CloudWatchAlarmScaler',
            'killStrategy'  => 'MostExpensiveFirst',
            'strategies' => [
                'OnDemand1' => ['type' => 'OnDemand', 'config' => $onDamand1Config],
                'OnDemand2' => ['type' => 'OnDemand', 'config' => $onDamand2Config]
            ],
            'instances'  => FakeInstanceConfig::$defaultConfig
        ];

        $groupConfig = new GroupConfig($config, $factoryMock);
        $groupConfig->getInstanceStrategies('some groups', $eventDispatcher);
        $groupConfig->getKillStrategy($instanceContext, $eventDispatcher);
    }

    public function testYamlConfig()
    {
        $factoryMock = $this->_getFactoryMock();

        $eventDispatcher = new EventDispatcher();
        $instanceContext = new FakeInstanceContext([
            'factory'         => $factoryMock,
            'eventDispatcher' => $eventDispatcher
        ]);

        $factoryMock->expects(self::exactly(2))
            ->method('getStrategyInstance')
            ->withConsecutive(
                [
                    'SimpleSpotBid',
                    [
                        'price' => '0.01',
                        'instanceConfig' => NULL,
                        'groupName' => 'group29',
                        'name' => 'dollar',
                        'eventDispatcher' => $eventDispatcher
                    ]
                ],
                [
                    'OnDemand',
                    [
                        'instanceConfig' => NULL,
                        'groupName' => 'group29',
                        'name' => 'always',
                        'eventDispatcher' => $eventDispatcher
                    ]
                ]
        );

        $factoryMock->expects(self::exactly(2))
            ->method('getInstanceConfig');

        $factoryMock->expects(self::exactly(1))
            ->method('getScalerInstance')
            ->with('CloudWatchAlarmScaler', ['alarmName' => 'testAlarm']);

        $factoryMock->expects(self::exactly(1))
            ->method('getKillStrategyInstance')
            ->with('MostExpensiveFirst', ['eventDispatcher' => $eventDispatcher, 'instanceContext' => $instanceContext]);

        $groupConfig = new GroupConfig(Config::load(__DIR__ . '/testConfig1.yml'), $factoryMock);
        $groupConfig->setFactory($factoryMock);
        $groupConfig->getInstanceStrategies('group29', $eventDispatcher);
        $groupConfig->getScaler();
        $groupConfig->getKillStrategy($instanceContext, $eventDispatcher);
    }

    /**
     * @return SparkyFactory|\PHPUnit_Framework_MockObject_MockObject
     */
    private function _getFactoryMock()
    {
        /** @var SparkyFactory|\PHPUnit_Framework_MockObject_MockObject $factoryMock */
        $factoryMock = self::getMockBuilder('\\Sparky\\SparkyFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $factoryMock->expects(self::atLeast(1))
            ->method('getClockInstance')
            ->will(self::returnValue(new Clock(Clock::MODE_TEST)));

        $testLogger = new Logger('sparky');
        $logHandler = new StreamHandler('php://stdout');
        $logHandler->setFormatter(new LineFormatter());
        $testLogger->pushHandler($logHandler);

        $factoryMock->expects(self::atLeast(1))
            ->method('getLogger')
            ->will(self::returnValue($testLogger));

        $factoryMock->expects(self::atLeast(1))
            ->method('getEventDispatcherInstance')
            ->will(self::returnValue(new EventDispatcher()));

        return $factoryMock;
    }
}

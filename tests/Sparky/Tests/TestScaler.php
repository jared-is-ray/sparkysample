<?php
namespace Sparky\Tests;

use Sparky\Scalers\ScalerInterface;

class TestScaler implements ScalerInterface
{
    /**
     * @var int
     */
    private $_capacity;

    /**
     * @var bool
     */
    private $_shutdown = FALSE;

    /**
     * @param int $capacity
     */
    public function setCapacity($capacity)
    {
        $this->_checkShutdown();
        $this->_capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getDesiredCapacity()
    {
        $this->_checkShutdown();
        return $this->_capacity;
    }

    /**
     * @return void
     */
    public function init()
    {
        $this->_checkShutdown();
    }

    /**
     * @return bool
     */
    public function isShutdown()
    {
        return $this->_shutdown;
    }

    public function shutdown()
    {
        $this->_checkShutdown();
        $this->_shutdown = TRUE;
    }

    private function _checkShutdown()
    {
        if($this->_shutdown)
        {
            throw new \LogicException(sprintf('%s already shutdown', __CLASS__));
        }
    }
}

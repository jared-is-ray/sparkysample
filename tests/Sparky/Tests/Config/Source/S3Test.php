<?php
namespace Sparky\Tests\Config\Source;

use Sparky\Config\Source\S3;
use Sparky\Tests\Helpers\S3MockHelper;
use Sparky\Tests\SparkyTestFactory;

class S3Test extends \PHPUnit_Framework_TestCase
{
    public function testIt()
    {
        $factory = new SparkyTestFactory();

        $contents1 = "hello: one";
        // invalid yaml
        $contents2 = "haslf";
        // valid yaml
        $contents3 = "hello: two";

        $s3Mocker = new S3MockHelper();
        $factory->setS3($s3Mocker->getS3Client());

        $s3Mocker->addResponse(200, $contents1, 'Tue, 31 Mar 2015 03:51:33 GMT');
        $s3Mocker->addResponse(200, '', 'Tue, 31 Mar 2015 03:51:33 GMT');
        $s3Mocker->addResponse(200, $contents2, 'Tue, 31 Mar 2015 03:55:33 GMT');
        $s3Mocker->addResponse(200, $contents3, 'Tue, 31 Mar 2015 03:57:33 GMT');

        $s3 = new S3([
            'uri'     => "s3://jray/test.yml",
            'factory' => $factory
        ]);

        self::assertEquals('one', $s3->getConfig()['hello']);
        self::assertFalse($s3->isChanged());
        self::assertEquals('one', $s3->getConfig()['hello']);
        self::assertFalse($s3->isChanged());
        self::assertEquals('one', $s3->getConfig()['hello']);
        self::assertTrue($s3->isChanged());
        self::assertEquals('two', $s3->getConfig()['hello']);
    }
}
